angular.module('admin').controller('AdminSettingsCtrl', function ($scope, $http, loading, admin) {
    loading.ready();
    admin.getData().then(function (admin) {
        $scope.admin = admin;
        $scope.invoiceData = $scope.admin.invoice_data;
    });

    $scope.postInvoice = function () {
        loading.loading();
        $http.post('/api/admin/admin', {invoice_data: $scope.invoiceData})
            .then(function () {
                loading.ready();
            });
    };
    $scope.postMailing = function () {
        loading.loading();
//        console.log($scope.admin.mailingPermission);
        $http.post('/api/admin/admin', {mailing_permission: $scope.admin.mailing_permission})
            .then(function () {
                loading.ready();
            });
    };
});
