angular.module('admin').controller('AddTabCtrl', function ($scope, $location, $http, loading, page) {
    loading.ready();

    $scope.$watch(function () {
        return $location.search().tab_type;
    }, function (newValue, oldValue) {
        $scope.addTab();
    });
    $scope.addTab = function () {
        if ($location.search().tab_type) {
            loading.loading();
            var s = $location.search();
            var url = '/api/pages/' + page.fbid + '/add_tab?tab_type=' + s.tab_type + '&subtype=' + s.subtype;
            $http.post(url).success(function () {
                $location.path("/page/tabs");
                $location.search('page_fbid=' + page.fbid);
            });
        }
    };
});