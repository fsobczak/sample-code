angular.module('admin').controller('PageTabsCtrl', function ($scope, $q, $http, loading, admin, fb, page) {
    $scope.fb = fb;
    $scope.onlySocialInvaders = true;
    var fetchSiTabs = function () {
        $http.get('/api/pages/' + page.fbid + '/tabs').then(function (resp) {
            $scope.siTabs = resp.data;
            fetchFbTabs();
        });
    };
    var fetchFbTabs = function () {
        fb.connected.then(function () {
            FB.api($scope.page.appendAT(page.fbid + '/tabs'), function (resp) {
                if (resp.error) { // manage_pages most likely - no valid token
                    fb.requireLogin('manage_pages', true).then(function () {
                        admin.updateBackendData().then(function () {
                            page.then(function () {
                                $scope.refreshAll();
                            });
                        });
                    });
                } else {
                    $scope.$apply(function () {
                        $scope.fbTabs = resp.data;
                        loading.ready();
                    });
                }
            });
        });
    };

    $scope.refreshAll = fetchSiTabs;
    $scope.refreshAll();
});
