angular.module('admin').controller('AdminCtrl', function ($scope, $location, $http) {
	$scope.location = $location;


	$scope.showSubNavbar = function () {
		return $location.path().match('^/page') !== null
		|| $location.path().match('cms') !== null
		|| $location.path().match('stats') !== null;
	};

	$scope.tabId = function () {
		return $location.search().tab_id;
	};
});