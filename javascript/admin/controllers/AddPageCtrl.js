angular.module('admin').controller('AddPageCtrl', function ($scope, $http, $location, admin, loading, fb) {
    loading.ready();
    $scope.add = {};
    fb.connected.then(function () {
        FB.api('/me/accounts', function (resp) {
            $scope.$apply(function () {
                $scope.fbPages = _.select(resp.data, function (page) {
                    return page.perms.indexOf('ADMINISTER') != -1;
                });
                loading.ready();
            });

        });
    });

    $scope.addPage = function () {
        loading.loading();
        var url = '/api/admin/add_page?page_fbid=' + $scope.add.page.id;
        $http.get(url).then(function () {
            $location.path("/page/tabs");
            $location.search('page_fbid=' + $scope.add.page.id);
        });
    }
});
