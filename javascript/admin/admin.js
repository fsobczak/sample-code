//= require_self
//= require_tree ./services
//= require_tree ./controllers
//= require_tree ./directives

google.load('visualization', '1', {packages: ['corechart']});

angular.module('admin', ['common', 'restangular', 'ngResource', 'ngRoute', 'ngAnimate', 'ngAnimate-animate.css',
    'googlechart.directives',
    'admin.edit',
    'admin.directives']);
angular.module('admin.directives', []);

var Resolve = {
    page: function (Page) {
        return Page.loadCurrent();
    },
    tab: function (Tab) {
        return Tab.loadCurrent();
    }
};

angular.module('admin')
.config(function ($routeProvider, $locationProvider, RestangularProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!');
    $routeProvider.when('/page/tabs', {
        templateUrl: '/tpl/page/tabs',
        reloadOnSearch: true,
        controller: 'PageTabsCtrl',
        resolve: {page: Resolve.page }
    });
    $routeProvider.when('/page/show', {
        templateUrl: '/tpl/page/show',
        reloadOnSearch: true,
        controller: 'PageShowCtrl',
        resolve: {page: Resolve.page }
    });

    $routeProvider.when('/page/contests', {
        templateUrl: '/tpl/page/contests',
        reloadOnSearch: true,
        resolve: {page: Resolve.page }

    });
    $routeProvider.when('/page/add_tab', {
        templateUrl: '/tpl/page/add_tab',
        controller: 'AddTabCtrl',
        reloadOnSearch: false,
        resolve: {page: Resolve.page }
    });
    $routeProvider.when('/admin/add_page', {
        templateUrl: '/tpl/admin/add_page',
        controller: 'AddPageCtrl',
        reloadOnSearch: false
    });
    $routeProvider.when('/admin/settings', {
        templateUrl: '/tpl/admin/settings',
        controller: 'AdminSettingsCtrl',
        reloadOnSearch: false}
        );
    $routeProvider.when('/tabs/:tab_type/edit', {
        templateUrl: function (params) {
            return '/tpl/tabs/' + params.tab_type + '/edit';
        },
        controller: 'EditTabCtrl',
        resolve: {tab: Resolve.tab },
        reloadOnSearch: false
    });

    $routeProvider.when('/admin/pages', {
        templateUrl: '/tpl/admin/pages',
        controller: 'AdminPagesCtrl'
    });

    RestangularProvider.setBaseUrl("/api");
    RestangularProvider.setRestangularFields({id: "_id"});

        // this is required, because json is sent for XML request
        RestangularProvider.setRequestInterceptor(function (elem, operation) {
            if (operation === "remove") {
                return undefined;
            }
            return elem;
        })
    })

.config(function ($httpProvider) {
$httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
})
.run(function ($rootScope, $location, $http, loading, fb) {

    if ($location.host() == 'dev.socialinvaders.com') {
        production = false
    }

    $rootScope.keys = function (arr) {
        if (!arr) return null;
        return Object.keys(arr);
    };


    $rootScope.range = function (len) {
        var ret = [];
        for (var i = 0; i < len; i++) {
            ret.push(i);
        }
        return ret;
    };
    $rootScope.$on('$routeChangeStart', function () {
        loading.loading();
    });
    $rootScope.$on('$routeChangeSuccess', function () {
        ga('send', 'pageview', $location.path());
    });
})
.filter('fb', function () {
    return function (fbid, args) {
        console.log(args);
        switch (args[0]) {
            case 'profile_link':
            return '<a href="http://facebook.com/profile.php?id=' + fbid + ' target="_blank">' + args[1] + '</a>';
            case 'profile_img':
            return '<img src="https://graph.facebook.com/' + fbid + '/picture?type=' + args[1] + '"/>';
        }
    }
})

.config(["$provide", function ($provide) {
    $provide.decorator("$templateCache", ["$delegate", "$http", "$injector", function ($delegate, $http, $injector) {
        var allDirectivesTplPromise;
        var loadDirectivesTemplates = function (url) {
            if (!allDirectivesTplPromise) {
                allDirectivesTplPromise = $http.get("/tpl/" + vars.locale + "/ng_templates")
                .then(function (response) {
                    $injector.get("$compile")(response.data);
                    return response;
                });
            }
            return allDirectivesTplPromise.then(function (response) {
                return {
                    status: response.status,
                    data: origGetMethod(url)
                };
            });
        };
        var origGetMethod = $delegate.get;
        $delegate.get = function (url) {
            if (url.indexOf('/') == -1) {
                return loadDirectivesTemplates(url);
            }
            return origGetMethod(url);
        };
        return $delegate;
    }]);
}])
.factory('pageCache', function ($cacheFactory) {
    return $cacheFactory('pageCache');
})
.factory('t',function ($filter) {
    return function (str) {
        return $filter('t')(str);
    }
}).run(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        }
    });
});
