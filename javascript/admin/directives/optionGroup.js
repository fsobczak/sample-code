angular.module('admin.directives')
    .directive('optionGroup', function ($filter, $interpolate) {
        return {
            restrict: 'E',
            scope: true,
            replace: true,
            transclude: true,
            templateUrl: 'option_group_template',
            link: function (scope, elm, attrs) {
                scope.visible = attrs.visible !== undefined;
                scope.name = $interpolate(attrs.name)(scope);
                if (!scope.name) scope.name = 'subpage';  // TODO solve race conditions with partials
                scope.label = $filter('t')('option_group.' + $interpolate(scope.name)(scope) + '.label');
//              scope.hint = $filter('t')('option-group.'+scope.name + '.hint');
                scope.trigger = function () {
                    scope.visible = !scope.visible;
                };
            }
        };
    });