angular.module('admin.directives')
    .directive('userSmall', function ($http, $interpolate, fb) {
        return {
            restrict: 'A',
            replace: true,
            transclude: true,
            scope: true,
            templateUrl: 'user_small_template',
            link: function (scope, elm, attrs) {

                var json = $interpolate(attrs.userSmall)(scope);
                if (json)
                    scope.user = $.parseJSON(json);
            }
        }
    });