angular.module('admin.directives')
    .directive('questionMark', function ($filter) {
        return {
            restrict: 'E',
            replace: true,
            template: "<span class='question-mark' rel='popover' data-content='{{hint}}' data-original-title='{{label}}'>[?]</span>",
            scope: true,
            link: function (scope, elm, attrs) {
                attrs.$observe('dict', function (value) {
                    scope.label = $filter('t')(attrs.dict + '.label');
                    scope.hint = $filter('t')(attrs.dict + '.hint');
                    if (!scope.hint || scope.hint.match('missing')) elm.hide();
                    else $(elm).popover({trigger: 'hover'});
                });
            }
        }
    });