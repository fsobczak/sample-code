angular.module('admin.directives')
    .directive('facebookTab', function ($http, fb) {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: true,
            templateUrl: 'facebook_tab_template',
            link: function (scope, elm, attrs) {
                if (scope.fbTab) {
                    if (scope.fbTab.application) {
                        scope.siTab = _.find(scope.siTabs, function (t) {
                            return t.global_params.fb.id == scope.fbTab.application.id;
                        });
                        if (scope.siTab) {
                            scope.siTab.active = true;
                        }
                    }
                }
                scope.visible = function () {
                    if (!scope.fbTabs) return false; // too soon, nei wiadomo czy siTab jest nieaktiv.
                    if (scope.onlySocialInvaders && !scope.siTab) return false;
                    if (!scope.fbTab && scope.siTab) {
                        return !scope.siTab.active;
                    }
//                    if (!scope.fbTab && !scope.siTab.active) return true;    // detached
                    return !(scope.fbTab.is_permanent || (scope.fbTab['application'] && scope.fbTab['application']['id'] == '2305272732'));
                };
                if (scope.siTab) {
                    scope.iconUrl = "/images/icons/tabs/" + scope.siTab.full_tab_type + ".png";
                }
                scope.changeTabName = false;
                scope.saveTabName = function () {
                    scope.updateTab({custom_name: scope.fbTab.name});
                    scope.changeTabName = false;
                };
                scope.updateTab = function (data) {
                    FB.api(scope.page.appendAT(scope.fbTab.id), 'post', data, function (resp) {
                    });
                };

                scope.installTab = function () {
                    scope.page.installTab(scope.siTab.global_params.fb.id).then(function () {
                        scope.fetchFbTabs();
                    });
                };

                scope.uninstallTab = function () {
//                    $http.delete(fb.pageLink(scope.fbTab.id)).then(function (resp) {
//                        console.log(resp);
//                    });
                    var modal = $('#delete-tab-modal');
                    if (scope.siTab) {
                        $('.socialinvaders', modal).show();
                        $('.not-socialinvaders', modal).hide();
                    } else {
                        $('.socialinvaders', modal).hide();
                        $('.not-socialinvaders', modal).show();
                    }
                    $('.name', modal).text(name);
                    $('.confirm', modal).unbind('click').click(function () {
                        scope.page.uninstallTab({tabPath: scope.fbTab.id}).then(function () {
//                            if (scope.fbTab) {
                            var index = scope.fbTabs.indexOf(scope.fbTab);
                            scope.fbTabs.splice(index, 1);
                            if (scope.siTab) scope.siTab.active = false;
                            $('.modal').modal('hide');
                        });
                    });
                    modal.modal();
                };

                scope.deleteTab = function () {
                    var modal = $('#delete-app-modal');
                    $('.name', modal).text(name);
                    $('.confirm', modal).unbind('click').click(function () {
                        var url = '/api/pages/' + scope.page_fbid + '/delete_tab?&tab_id=' + scope.siTab._id;
                        $http.get(url).then(function () {
                            scope.refreshAll();
                        });
                        scope.$apply();
                        $('.modal').modal('hide');
                    });
                    modal.modal();
                };
            }
        }
    })