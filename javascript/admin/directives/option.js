angular.module('admin.directives')
    .directive('option', function () {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: false,
            templateUrl: 'option_template',
            link: function (scope, elm, attrs) {
                $('.name', elm).html(attrs.name);
            }
        }
    });