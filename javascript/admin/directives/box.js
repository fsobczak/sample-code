angular.module('admin.directives')
    .directive('box', function () {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            scope: true,
            templateUrl: 'box_template',
            link: function (scope, elm, attrs) {
                scope.attrs = attrs;
            }
        }
    })