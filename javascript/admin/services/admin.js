angular.module('admin')
    .service('admin', function ($q, $http, $rootScope, $location, Page, Tab, fb) {
        var self = this;
        this.updateBackendData = function () {
            var deferred = $q.defer();
            $http.post('/api/admin/update_pages').then(function (resp) {
                deferred.resolve(resp.data);
            });
            return deferred.promise;
        };
        this.getData = function () {
            var deferred = $q.defer();
            $http.get('/api/admin/admin').then(function (resp) {
                deferred.resolve(resp.data);
            });
            return deferred.promise;
        };

        this.link = function (url, params) {
            var ret = url;
            if (url.indexOf('?') == -1) ret += '?';
            ret += '&tab_id=' + vars.tab_id;
            if (params) {
                for (key in params) {
                    ret += '&' + key + '=' + params[key];
                }
            }
            return ret;
        };
    });
