angular.module('admin')
    .factory('Tab', function ($rootScope, $http, $q, $location, fb, Page) {
        function Tab(tab_id) {
            var deferred = $q.defer();
            var self = this;
            $http.get('/api/tabs/' + tab_id).then(function (resp) {
                _.extend(self, resp.data);
                deferred.resolve(self);
            });
            return deferred.promise;
        }

        Tab.loadCurrent = function () {
            tab_id = $location.search().tab_id;
            if ($rootScope.tab && $rootScope.tab.id == tab_id) return; // already loaded this tab
            if (!tab_id) throw 'no tab_id';
            return new Tab(tab_id).then(function(tab) {
                var deferred = $q.defer();
                $rootScope.tab = tab;
                Page.loadCurrent().then(function () {
                    deferred.resolve(tab);
                });
                return deferred.promise;
            });
        };
        return Tab;
    });
