angular.module('admin')
    .factory('Page', function ($rootScope, $http, $q, $location, fb) {
        var last_page = null;

        function Page(page_fbid) {
            var page_fbid = page_fbid;
            var self = this;
            last_page = {fbid: page_fbid};

            this.isInstalled = function (appId) {
                var deferred = $q.defer();
                FB.api(this.appendAT(this.fbid + '/tabs/' + appId), 'get', function (resp) {
                    deferred.resolve(resp.data.length > 0);
                });
                return deferred.promise;
            };

            this.uninstallTab = function (params) {
                var deferred = $q.defer();
                var tabPath = params.appId ? this.fbid + '/tabs/app_' + params.appId : params.tabPath;
                FB.api(this.appendAT(tabPath), 'delete', function (resp) {
                    $rootScope.$apply(function () {
                        deferred.resolve();
                    });
                });
                return deferred.promise;
            };

            this.installTab = function (tab_fbid) {
                var deferred = $q.defer();
                FB.api(self.appendAT(self.fbid + '/tabs'), {app_id: tab_fbid}, 'post', function (resp) {
                    $rootScope.$apply(function () {
                        if (resp == true) {
                            deferred.resolve();
                        } else {
                            deferred.reject();
                        }
                    });
                });
                return deferred.promise;
            };

            this.access_token = null;

            this.appendAT = function (url) {
                return url + '?access_token=' + self.access_token;
            };
//            this.apiLink = function (path) {
//                return fb.fbGraphLink(path, self.access_token);
//            };
            return $http.get('/api/pages/' + page_fbid).then(function (resp) {
                _.extend(self, resp.data);
                last_page = self;
                self.access_token = resp.data['access_token'];
                $rootScope.pages = resp.data['pages'];
                return self;
            });
        }

        Page.loadCurrent = function () {
            var page_fbid = $location.search().page_fbid;
            if (!page_fbid) {
                page_fbid = $rootScope.tab.page_id;
            }
            if (!page_fbid) return $q.reject();
            else {
                if (last_page && page_fbid == last_page.fbid) {
                    return $q.when(last_page);
                }
                return new Page(page_fbid).then(function (page) {
                    $rootScope.page = page;
                    return page;
                });
            }
        };
        return Page;
    });
