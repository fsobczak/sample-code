//= require jquery.min
//= require jquery.ui.core
//= require jquery.ui.widget
//= require angular/angular
//= require angular/angular-sanitize.min
//= require angular/angular-route.min
//= require bootstrap.min
//= require lodash.min
//= require_self
//= require tabs/_directives
//= require common
production = false;

angular.module('tab', ['common', 'ngSanitize', 'ngRoute'])
    .filter('fb.rb', function () {
        return function (fbid, args) {
            switch (args[0]) {
                case 'profile_link':
                    return '<a href="http://facebook.com/profile.php?id=' + fbid + ' target="_blank">' + args[1] + '</a>';
                case 'profile_img':
                    return '<img src="https://graph.facebook.com/' + fbid + '/picture?type=' + args[1] + '"/>';
            }
        }
    })
    .factory('app', function ($q, $rootScope, $http, $compile, $location, fb) {
        return new function () {
            var self = this;
            this.vars = vars;

            this.invite = function () {
                var message = vars.invite.message || 'message';
                var title = vars.invite.title || 'title';
                FB.ui({
                    method: 'apprequests',
                    message: message,
                    title: title,
                    data: vars.tab_id
                }, function (response) {
                    if (response) {
                        var list = response.to.join(',');
                        $.getJSON(self.link('/ajax/invite'), {
                            request: response.request,
                            list: response.to.join(',')
                        }, function (d) {
                            $('#modal-msg').modal('hide');
                        });
                    }
                });
            };

            this.modalMsg = function (modal_msg) {
                $rootScope.modal_msg = modal_msg;
                $('#modal-msg-body').html(modal_msg.body);
                $compile($('#modal-msg-body'))($rootScope);
                $('#modal-msg').modal();
            };

            this.authData = function (data) {
                data['tab_type'] = this.vars['tab_type'];
                data['tab_id'] = this.vars['tab_id'];
                return data;
            };
            this.refreshTab = function () {
                top.location = this.vars.tabUrl;
            };
            this.link = function (url, params) {
                var ret = '/tabs/' + this.vars['tab_type'];
                ret += url;
                if (url.indexOf('?') == -1) ret += '?';
                if (!params) params = {};
                params.tab_id = this.vars.tab_id;
                params.tab_slot = this.vars.tab_slot;
                if (fb.accessToken)
                    params.access_token = fb.accessToken;
                for (key in params) {
                    ret += '&' + key + '=' + params[key];
                }
                return ret;
            };
            this.getTabUser = function () {
                var deferred = $q.defer();
                if (self.user) {
                    $q.defer().resolve(self.user);
                } else {
                    $http.get(self.link('/api/tab_user'))
                        .then(function (resp) {
                            deferred.resolve(resp.data);
                        });
                }
                return deferred.promise;
            };

            // this works better than dynamic routing, because there's no transition time (empty ng-view)
            this.loadMain = function (path) {
                var msec = 200;
                var len = 15;
                var complete = $q.defer();
                $('#main').animate({'left': -len, opacity: 0, 'background-position': '20px 30px'}, msec, function () {
                    complete.resolve();
                    $rootScope.$apply();
                });
//            $("html, body").animate({ scrollTop: 200 }, "slow");
                $('#menu a').removeClass('active');
                $('#menu a[href="#!' + path + '"]').addClass('active');

                $http.get(this.link(path)).then(function (resp) {
                    var html = resp.data;
                    complete.promise.then(function () {
                        $('#main').css('left', len).css('opacity', 0); //, opacity: 0});
                        var main = $('#main');
                        main.html(html);
                        $compile(
                            main
                        )($rootScope);
                        $('#main').animate({'left': '0px', opacity: 1, 'background-position': '50px 30px'}, 300);
                    });
                });
            };
            //  dynamic one-page apps
            if (this.vars.tab_type == 'answer_contest' || this.vars.tab_type == 'memo_contest') {
                $rootScope.$watch(function () {
                    return $location.path();
                }, function (newValue, oldValue) {
                    if (newValue == '' || newValue == '/main') newValue = '/main/start';
                    self.loadMain(newValue);
                });
            }
            if (this.vars.path) {
                $location.path(this.vars.path);
            }
        }();
    })
    .factory('viral', function () {
        return new function () {
            this.shareInner = function (d) {
                var params = {
                    method: 'feed',
                    name: d.name,
                    link: d.link,
                    picture: d.picture,
                    caption: d.caption,
                    description: d.description
                };
                FB.ui(params, function (response) {
                        if (response && response.post_id) {
//                            alert('Post was published.');
                        }
                    }
                );
            };
        };
    })
    .run(function ($rootScope, $q, $timeout, app, fb) {
        $.ajaxSetup({
            beforeSend: function (xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        });
        fb.init.then(function () {
            FB.Event.subscribe('edge.create', function (href, widget) {
                $('#fangate-modal').fadeOut();
            });
        });
        $rootScope.vars = app.vars;
        $rootScope.app = app;
        var initControllers = function () {
            $('.dummy-image').each(function (k, div) {
                div = $(div);
                var path = div.data('path');
                var data = app.authData({ path: path});
                $('.ok', div).click(function () {
                    $.post("/tab_admin/hide_dummy_image", data);
                    div.fadeOut();
                    return false;
                });
            });
            $('#top-panel .dismiss').click(function () {
                $('#top-panel').animate({opacity: 0}).slideUp();
                return false;
            });
        };
        initControllers();
    });


