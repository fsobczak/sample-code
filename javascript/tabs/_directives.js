angular.module('tab').directive('dropdown', function () {
    return function (scope, elm, attrs) {
        var lis = $('li', elm);
        var current = $('.current', elm);
        lis.click(function () {
            current.html($('a', this).html());
            lis.show();
        });
    }
});

angular.module('tab').directive('contestTiming', function ($interpolate, app, util) {
    return {
        restrict: 'C',
        scope: true,
        link: function (scope, elm, attrs) {
            var initTimer = function () {
                function updateTime() {
                    var now = new Date();
                    var time = (app.vars.contest.status == 'running') ? app.vars.contest.finish : app.vars.contest.start;
                    var milli = new Date(time) - new Date();
                    var timeleft = new Date(milli);
                    var span = $('#time-left');
                    var daysleft = (timeleft / 86400000).toFixed(0);
                    scope.$apply(function () {
                        scope.days = daysleft;
                        scope.time = timeleft.getUTCHours() + ':' + util.pad(timeleft.getUTCMinutes(), 2)
                            + ':' + util.pad(timeleft.getUTCSeconds(), 2);
                    });
                }
                var timer = window.setInterval(updateTime, 1000);
            };
            if (app.vars.contest.status == 'preparation' || app.vars.contest.status == 'running') {
                initTimer();
            }
        }
    }
});
