$(function() {
    var directionDisplay;
    var directionsService = new google.maps.DirectionsService();
    var marker = null;
    function setMarker(pos) {
        if (!marker) {
            marker = new google.maps.Marker({
                map:map,
                position:pos,
                draggable:true
            });
        } else {
            marker.setPosition(pos);
        }
        map.setCenter(pos);
    }

    var myOptions = {
        zoom: 7,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng(41.850033, -87.6500523)
    };

    function initialize() {
        directionsDisplay = new google.maps.DirectionsRenderer();
        var div = $('.map');
        var latLng = div.data('latlng');
        var zoom = div.data('zoom');
        if (latLng) {
            latLng = latLng.split(',');
            myOptions['center'] = new google.maps.LatLng(parseFloat(latLng[0]), parseFloat(latLng[1]));
        }
        latLng = myOptions['center'];
        myOptions['zoom'] = zoom;

        map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
        directionsDisplay.setMap(map);
        setMarker(latLng);

        var control = $('#control')[0];
        control.style.display = 'block';
        map.controls[google.maps.ControlPosition.BOTTOM].push(control);
    }

    // global
    calcRoute = function() {
        var start = $('#start').val();
        var div = $('.map');
        var latLng = div.data('latlng');
        var end = latLng;
        var request = {
            origin:start,
            destination:end,
            travelMode:google.maps.DirectionsTravelMode.DRIVING
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                marker.setMap(null);
                directionsDisplay.setDirections(response);
            }
        });
    }

    google.maps.event.addDomListener(window, 'load', initialize);

});
