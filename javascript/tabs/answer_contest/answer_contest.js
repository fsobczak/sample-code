//= require jquery.ui.slider
//= require jquery.ui.widget
//= require jquery.ui.datepicker
//= require jquery.slides.min
//= require jquery.masonry.min
//= require jquery.textfill.min
//= require tabs/_contest
//= require_self
//= require upload

ngApp = angular.module('tab')
    .config(function ($locationProvider, $routeProvider) {
        $locationProvider.html5Mode(false);
        $locationProvider.hashPrefix('!');
    })
    .factory('answers',function ($cacheFactory, $q, $http, app) {
        return new function () {
            var self = this;
            this.list = null;
            this.getList = function () {
                var deferred = $q.defer();
                if (self.list) {
                    deferred.resolve(self.list);
                } else {
                    $http.get(app.link('/api/answers'))
                        .then(function (resp) {
                            self.list = resp.data;
                            deferred.resolve(self.list);
                        });
                }
                return deferred.promise;
            };
            this.refreshList = function () {
                this.list = null;
            };
        }();
    }).directive('answer', function ($http, $rootScope, $filter, fb, app, og) {
        return {
            restrict: 'A',
            scope: true,
            link: function (scope, elm, attrs) {
                scope.vote = function () {
                    var btn = $('.vote', elm);
                    fb.requireLogin().then(function () {
                        $http.get(app.link('/ajax/vote?id=' + scope.answer._id)).then(function (resp) {
                            var data = resp.data;
                            if (data.modal_msg) {
                                app.modalMsg(data.modal_msg);
                            }
                            if (data.ok) {
                                og.post('me/inv_answercontest:vote', {object: scope.answer.opengraph_url});
                                btn.html($filter('t')('thank_you!'));
                                btn.addClass('disabled');
                            }
                            scope.answer.votes_count = data.votes_count;
                        });
                    });
                };
                scope.like = function () {
                    fb.requireLogin().then(function () {
                        $http.get(app.link('/ajax/toggle_like?id=' + scope.answer._id)).then(function (resp) {
                            var data = resp.data;
                            if (data.ok) {
                                scope.answer.liked = data.liked;
                                scope.answer.likes_count = data.likes_count;
                                if (data.liked) {
                                    og.post('me/og.likes', {object: scope.answer.opengraph_url});
                                }
                            }
                        });
                    });
                };
                scope.share = function () {
                    FB.ui(
                        {
                            method: 'feed',
                            link: scope.answer.opengraph_url
                        },
                        function (response) {
                        }
                    );

                };
            }
        };
    })
    .run(function ($rootScope, $location, $compile, app) {
        if (app.vars.sr_data && app.vars.sr_data.app_data) {
            $location.path('/main/view/' + app.vars.sr_data.app_data);
        }
    }
);

function AddCtrl($scope, $compile, $filter, $http, app, fb, answers, og) {
    var answerClasses = app.vars.answer_classes;

    var div = $('#add');
    $scope.video = null;
    $scope.video_url = "";
    $scope.previewLoading = false;
    $scope.explicitlyShared = true;
    $scope.acceptTerms = true;

    $scope.requestAdd = function (cls) {
        fb.requireLogin().then(function () {
//            console.log('ok req login git!');
            $.getJSON(app.link('/ajax/add?selected_class=' + cls)).then(
                function (d) {
                    $scope.selectedClass = cls;
                    $('#add').html(d.html);
                    $compile($('#add'))($scope);
                    $scope.enableAdd();
                    $scope.$apply();
                    answers.refreshList();

                    if (d.action_allowed) {
                        $('#request-add').hide();
                        $('#add').slideDown();
                    }
                }
            );
        }, function () {
//            console.log('REQ LOGIN FAIL!!!!');
        });
    };

    $scope.enableAdd = function () {
        if (_(answerClasses).contains('video')) {
            var timeout;
            var canSend = false;
            var preview = function () {
                $('#error').slideUp();
                $('#preview').slideUp();
                var url = $('#add input').val();
                $scope.previewLoading = true;
                $.getJSON(app.link('/ajax/preview_youtube?url=' + encodeURIComponent(url)), function (d) {
                    $scope.previewLoading = false;
                    $scope.video = d;
                    $scope.$apply();
                    $('#add button').show();
                    canSend = true;
                });
            };
            $('#add input').on('paste input', function () {
                clearTimeout(timeout);
                timeout = setTimeout(preview, 1000);
            });
        }
        if (_(answerClasses).contains('image')) {
            $('.fileinput-button input').fileupload({
                dataType: 'json',
                url: app.link('/ajax/upload'),
                add: function (e, data) {
                    $scope.previewLoading = true;
                    $scope.$apply();
                    data.submit();

                },
                done: function (e, data) {
                    $scope.previewLoading = false;
                    if (data.result.error) {
                        $scope.error = data.result.error;
                        $scope.$apply();
                    } else {
                        $.each(data.result, function (index, file) {
                            $scope.file = $.extend({}, file);
                            $scope.$apply();
                        });
                    }
                },
                fail: function (e, data) {
                    $scope.previewLoading = false;
                }
            });
        }
    };

    $scope.post = function () {
        var p = {};
        p.text = $('textarea', div).val();
        p.selected_class = $scope.selectedClass;
        p.explicitly_shared = $scope.explicitlyShared;


        if ($scope.selectedClass == 'video') {
            p.url = $('input', div).val();
        } else if ($scope.selectedClass == 'image') {
            if (!$scope.file) {
                $scope.error = $filter('t')('answer_contest.errors.upload_image_before_adding');
                return;
            }
            p.file_id = $scope.file.file_id;
            p.url = $scope.file.url;
            p.thumb_url = $scope.file.thumb_url;
            p.original_url = $scope.file.original_url;
        }

        if (!$scope.acceptTerms) {
            $scope.error = $filter('t')('answer_contest.errors.accept_terms_required');
            return;
        }
        $http.post(app.link('/ajax/add'), p).then(function (resp) {
            var data = resp.data;
            var answer = data.answer;
//            console.log(answer);
            og.post('me/og.posts', {object: answer.opengraph_url, 'fb:explicitly_shared': 'true'});
            $('#add').slideUp(function () {
                $('#add-thankyou').slideDown();
                FB.Canvas.scrollTo(0, 0);
            });
        });
    };
}

function ViewCtrl($scope, $timeout, $rootScope, $location, answers) {
    var path = $location.path();
    var spl = path.split('/');
    var answer_id = spl[spl.length - 1];

    $scope.renderYoutube = function () {
        if (!$scope.answer || !$scope.answer.youtube_video) return;
        // najpierw answers zapełnić
        return '<iframe allowfullscreen="" frameborder="0" height="350" width="510" src="http://www.youtube.com/embed/'
            + $scope.answer.youtube_video.unique_id + '?wmode=transparent&autoplay=1" wmode="transparent"></iframe>'
    };

    $scope.initAnswer = function () {
        $scope.answer = _.find($scope.answers, function (a) {
            return a._id == answer_id;
        });
        $scope.index = $scope.answers.indexOf($scope.answer);
        $scope.onAnswerChanged();
    };

    $scope.onAnswerChanged = function () {
        if ($scope.answer.text_only) {
            $timeout(function () {
                $('.textfill').textfill({ maxFontPixels: 36 });
            }, 0);
        }
        if (typeof(FB) !== undefined) {
            $timeout(function () {
                FB.XFBML.parse($('#view')[0]);
            }, 0);
        }
    };

    if ($rootScope.sortedList) {
        $scope.answers = $rootScope.sortedList;
        $scope.initAnswer();
    } else {
        answers.getList().then(function (answers) {
            $scope.answers = answers;
            $scope.initAnswer();
        });
    }

    $scope.next = function () {
        $scope.index = ($scope.index + 1) % $scope.answers.length;
        $scope.answer = $scope.answers[$scope.index];
        $scope.onAnswerChanged();
    };
    $scope.prev = function () {
        $scope.index = ($scope.index - 1 + $scope.answers.length) % $scope.answers.length;
        $scope.answer = $scope.answers[$scope.index];
        $scope.onAnswerChanged();
    };
}

function AnswersCtrl($scope, $rootScope, $timeout, $location, fb, app, answers, ListWrapper) {
    $scope.list = new ListWrapper();
    $scope.list.itemsPerPage = 12;
    $('#answers').masonry({itemSelector: '.item'});
    $scope.list.afterSearch = function () {
        $timeout(function () {
            $('#answers').imagesLoaded(function () {
//                $('#answers').masonry('option', {isAnimated: false});
                $('#answers').masonry('reload');
            });
        }, 0);
        $rootScope.sortedList = $scope.list.filteredItems;
    };

    $scope.list.afterPageChanged = function () {
        $timeout(function () {
            $('#answers').imagesLoaded(function () {
                $('#answers').masonry('reload');
            });
        }, 0);
    };

    answers.getList().then(function (list) {
        $scope.unfilteredItems = list;
        if ($location.search().filter) {
            $scope.filterBy($location.search().filter);
        } else {
            $scope.filterBy('none');
        }


    });
    $scope.sort = 'created_at';

    $scope.sortBy = function (newSortingOrder) {
        $scope.sort = newSortingOrder;
        $scope.list.sortBy(newSortingOrder, true);
    };

    fb.connected.then(function () {
        app.getTabUser().then(function (tab_user) {
            var likedIds = _(tab_user.likes).pluck('answer_id');

            _($scope.unfilteredItems).each(function (item) {
                if (_(likedIds).contains(item._id)) {
                    item.liked = true;
                }
            });
        });
    });

    $scope.filterBy = function (filter) {
        $scope.filter = filter;
        if (filter == 'none') {
            $scope.list.items = $scope.unfilteredItems;

            $scope.list.query.text = '';
            $scope.list.search();
        } else if (filter == 'winners') {
            $scope.list.items = _($scope.unfilteredItems).filter(function (item) {
                return item.winner;
            });
            $scope.sort = 'winner';
            $scope.list.query.text = '';
            $scope.list.search();
        } else if (filter == 'friends') {
            fb.requireLogin().then(function () {
                fb.getFriendsIds().then(function (friendsIds) {
                    $scope.list.items = _($scope.unfilteredItems).filter(function (item) {
                        return _.contains(friendsIds, item.tab_user.user.fbid);
                    });
                    $scope.list.query.text = '';
                    $scope.list.search();
                });
            });
        } else if (filter == 'my') {
            fb.requireLogin().then(function () {
                $scope.list.items = _($scope.unfilteredItems).filter(function (item) {
                    return fb.userID == item.tab_user.user.fbid;
                });
                $scope.list.query.text = '';
                $scope.list.search();
            });
        }
    };
    $scope.view = function (id) {
        $location.path('/main/view/' + id);
    };
}
