SocialInvaders::Application.routes.draw do

  mount RailsAdmin::Engine => '/radmin', :as => 'rails_admin' if defined? RailsAdmin
  mount Ckeditor::Engine => "/ckeditor"

  class OnlyNonAjaxRequest
    def matches?(request)
      !request.xhr?
    end
  end

  root :to => 'web#index'

  match '/l/:tab_id', to: 'tabs/main#redirect'

  scope 'api', :module => 'api' do
    namespace 'tabs' do
      scope 'answer_contest/:tab_id', :module => 'answer_contest' do
        resources :answers do
        end
      end
      scope 'memo_contest/:tab_id', :module => 'memo_contest' do
        resources :games
      end
    end
  end

  namespace 'api' do
    scope 'pages/:page_id' do
      get '', to: 'pages#show'
      get 'tabs', to: 'pages#tabs'
      match 'add_tab', to: 'pages#add_tab'
      match 'delete_tab', to: 'pages#delete_tab'
    end
    scope 'tabs/:tab_id' do
      get '', to: 'tabs#show'
    end
  end

  match ':controller(/:action(/:id))', controller: /api\/[^\/]+/

  scope 'tpl' do
    match ':controller(/:action(/:id))'

    match 'tabs/:tab_type/edit', to: 'edit_tab#edit'

    match ':controller(/:action(/:id))', :controller => /tabs\/memo_contest\/cms\/[^\/]+/
    match ':controller(/:action(/:id))', :controller => /tabs\/answer_contest\/[^\/]+/

    match ':controller(/:action(/:id))', :controller => /tabs\/answer_contest\/cms\/[^\/]+/
    match ':controller(/:action(/:id))', :controller => /tabs\/answer_contest\/[^\/]+/
  end

  match 'tabs/*a/cms/*b', to: 'admin#index'
  match 'tabs/*a/stats', to: 'admin#index'
  match 'tabs/*a/edit', to: 'edit_tab#index'

  match 'admin/*p', to: 'admin#index'
  match 'page/*p', to: 'admin#index'


  scope "tpl/:locale", :locale => /en|pl/ do
    match '(:action(/:id))(.:format)', :controller => 'web'
  end

  scope "/:locale", :locale => /en|pl/ do
    get '*path', to: 'web#index'
    get '', to: 'web#index'
  end

  match ':controller(/:action(/:id))', :controller => /tabs\/[^\/]+/

  match ':controller(/:action(/:id))', :controller => /alien\/[^\/]+/


  match ':controller(/:action(/:id))', :controller => /tabs\/youtube\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/contact\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/static\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/redirect\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/map\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/topfans\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/files\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/twitter\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/coupons\/[^\/]+/


  match ':controller(/:action(/:id))', :controller => /tabs\/instagram_tab\/[^\/]+/

  match ':controller(/:action(/:id))', :controller => /tabs\/answer_contest\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/memo_contest\/[^\/]+/
  match ':controller(/:action(/:id))', :controller => /tabs\/instagram_contest\/[^\/]+/


  match ':controller(/:action(/:id))'

end
