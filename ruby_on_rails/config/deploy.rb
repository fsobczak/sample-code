$:.unshift(File.expand_path('./lib', ENV['rvm_path']))
$:.unshift('/home/social/.rvm/gems/ruby-1.9.3-p429/bin')
set :rvm_ruby_string, '1.9.3'
set :rvm_type, :user

require 'capistrano/ext/multistage'

default_run_options[:pty] = true
set :application, "social_invaders"
set :repository,  "ssh://social@socialinvaders.com/home/social/si.git"

set :scm, :git

set :user, "social"
#role :db,  "your primary db-server here", :primary => true # This is where Rails migrations will run
#role :db,  "your slave db-server here"

set :stages, ["staging", "production"]
set :default_stage, "staging"

set :uploads_dirs, %w(public/uploads public/upload public/gen-less) 
set :shared_children, fetch(:shared_children) + fetch(:uploads_dirs)

namespace :deploy do
  task :restart, :roles => :web do
    run "touch #{ current_path }/tmp/restart.txt"
  end

  #task :restart_daemons, :roles => :app do
  #  sudo "monit restart all -g daemons"
  #end
end
set :use_sudo, false

set :deploy_via, :checkout


after "deploy:restart", "deploy:cleanup"

namespace :deploy do
  desc "Push local changes to Git repository"
  task :push do

    # Check for any local changes that haven't been committed
    # Use 'cap deploy:push IGNORE_DEPLOY_RB=1' to ignore changes to this file (for testing)
    status = %x(git status --porcelain).chomp
    if status != ""
      if status !~ %r{^[M ][M ] config/deploy.rb$}
        raise Capistrano::Error, "Local git repository has uncommitted changes"
      elsif !ENV["IGNORE_DEPLOY_RB"]
        # This is used for testing changes to this script without committing them first
        raise Capistrano::Error, "Local git repository has uncommitted changes (set IGNORE_DEPLOY_RB=1 to ignore changes to deploy.rb)"
      end
    end

    # Check we are on the master branch, so we can't forget to merge before deploying
    branch = %x(git branch --no-color 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \\(.*\\)/\\1/').chomp
    if branch != "master" && !ENV["IGNORE_BRANCH"]
      raise Capistrano::Error, "Not on master branch (set IGNORE_BRANCH=1 to ignore)"
    end

    # Push the changes
    if ! system "git push #{fetch(:repository)} master"
      raise Capistrano::Error, "Failed to push changes to #{fetch(:repository)}"
    end

  end
end

if !ENV["NO_PUSH"]
  before "deploy", "deploy:push"
  before "deploy:migrations", "deploy:push"
end
