module AdminHelper

  def include_ng_templates(list)
    ret = ''
    list.each do
      ret += "<script type='text/ng-template' id=#{id}_template>"
      ret += render :text => "admin/ng/#{id}"
    end
    raise
  end

  def find_texts(l)
    if l[:dict] == :local
      l[:label] = I18n.t(l[:path]+'.label', :scope => @co.tab.tab_type)
      l[:hint] = I18n.t(l[:path]+'.hint', :default => '', :scope => @co.tab.tab_type)
    else
      l[:label] = I18n.t(l[:path]+'.label') unless l[:label]
      l[:hint] = I18n.t(l[:path]+'.hint', :default => '') unless l[:hint]
    end
  end

  def dir_tabs(locals = {})
    raw render :partial => 'wa/dir_tabs', :locals => locals
  end

  def wa(cmd, l = {})
    l[:value] = Util.path_val(l[:path]) if !l[:value] && %w(color button_look map).include?(cmd)
    l[:cls] = '' unless l[:cls]
    raw render :partial => 'wa/'+cmd, :locals => l
  end

end