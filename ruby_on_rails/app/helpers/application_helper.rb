module ApplicationHelper
  def w(cmd, l = {})
    raw render :partial => 'w/'+cmd, :locals => l
  end

  # local widget
  def lw(cmd, l = {})
    co = RequestStore.store[:co]
    raw render :partial => "/tabs/answer_contest/lw/#{cmd}", :locals => l
  end
end