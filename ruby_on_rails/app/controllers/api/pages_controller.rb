class Api::PagesController < Api::BaseController

  def tabs
    @list = @co.page.tabs
    render :json => @list
  end

  def show
    render :json => @co.page
  end

  #def page
    #render :json => @co.page
  #end

  def add_tab
    tab_type = params[:tab_type]
    tab_slot = GlobalTabParams.find_free_tab_slot(@co.page, tab_type)
    if tab_slot
      tab = Tab.create_tab(tab_type, tab_slot, :page => @co.page, :subtype => params[:subtype])
      # page is from page_fbid, init at @co
      #@co.page.install_tab(tab)
      @co.page.refresh_tabs
      ret = [:ok => true]
    else
      ret = [:error => 'too many slots']
    end
    render :json => ret.to_json
  end

  def delete_tab
    tab = Tab.find(params[:tab_id])
    tab.delete
    ret = [:ok => true]
    render :json => ret.to_json
  end
end