class Api::Tabs::AnswerContest::AnswersController < Api::BaseController
  layout nil

  def index
    render :json => @co.tab.answers
  end

  private
  def find_answer
    @co.tab.answers.detect { |a| a.id.to_s == params[:id] }
  end

  public
  # POST
  def create # this also serves for update in angularjs
    render :json => {'ok' => true}
  end


  def show
    render :json => find_answer.to_json
  end

  # PUT
  def update
    answer = find_answer
    answer.winner = params[:winner]
    answer.save!
    render :json => {'ok' => true}
  end

  # DELETE
  def destroy
    find_answer.delete
    render :json => {ok: true}
  end

end