class Api::AdminController < ApplicationController
  layout nil


  def add_page
    ret = {}
    if params[:page_fbid]
      page_fbid = params[:page_fbid]
      page = Page.create_from_fbid(@admin, page_fbid)
      ret['ok'] = true
      #redirect_to page.admin_url('tabs')
    end
    render :json => ret
  end

  def update_pages
    @co.admin.fetch_pages
    render :json => {ok: true}
  end

  def register
    @fb.fb_auth.exchange_token! params[:access_token]
    admin = Admin.create_with_access_token(@fb.fb_auth.access_token, {remote_ip: request.remote_ip})
    admin.mailing_permission = params[:mailing_permission]
    render :json => {ok: true}
  end

  def admin
    #@co.admin.invoice_data = InvoiceData.new
    #@co.admin.invoice_data.save!
    if request.post?
      if params[:invoice_data]
        #raise params[:invoice_data].inspect
        @co.admin.invoice_data = InvoiceData.new(params[:invoice_data])
        @co.admin.invoice_data.save!

      elsif params[:mailing_permission]
        #raise params.inspect
        @co.admin.mailing_permission = params[:mailing_permission] == 'true'
        @co.admin.save!
      end
    end
    render :json => @co.admin.to_json
  end

  def save_instagram_access_token
    @co.admin.instagram_access_token  = params[:access_token]
    @co.admin.save!
    render :json => {ok: true}
  end

end