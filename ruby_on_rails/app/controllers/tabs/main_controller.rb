class Tabs::MainController < ApplicationController
  def redirect
    tab = Tab.find(params[:tab_id])
    if is_mobile?
      redirect_to tab.tab_url(nil, false, 'mobile')
    else
      l = {
          redirect_url: tab.tab_url,
          metadata: tab.opengraph_meta
      }
      render :partial => 'helper/opengraph', :locals => l
    end
  end

  def canvas
    if params[:request_ids]                 # from invite
      request_id = params[:request_ids].split(',')[0]
      inv = Invite.where(request: request_id).first
      @tab = inv.tab

      render :text => "<script>window.top.location.href = '#{@tab.universal_link}';</script>";
    else
      render :text => "<script>window.top.location.href = 'https://socialinvaders.com';</script>";
    end
  end
end
