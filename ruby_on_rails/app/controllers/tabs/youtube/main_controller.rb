class Tabs::Youtube::MainController < GMainController
  def index
    list = @co.tab.videos
    v = list.first
    #@co.fb
    #if (isset(FB::$ad['id'])) {
    #    //            print_r(FB::$ad); exit;
    #$v = Video::model()->findByPk(new MongoID(FB::$ad['id']));
    #}
    #if (!$v) $v = $list[0];
    #$this->render('index', array('v' => $v, 'list' => $list));
    if @co.tab.design.layout['type'] == 'minimal'
      render 'minimal', :locals => {:list => list, :v => v}
    else
      render 'index', :locals => {:list => list, :v => v}
    end
  end

  def opengraph
    unique_id = params[:unique_id]
    v = @co.tab.videos.select {|x| x.unique_id == unique_id }
    v = v[0]
    og_url = @co.tab.video_opengraph_url(unique_id)
    metadata = [
        "fb:app_id" => tab.global_params[:fb][:id],
        "og:description" => v.description,
        "og:title" => v.title,
        "og:image" => v.thumbnail_url('hq'),
        "og:url" => og_url
    ]
    render 'system_tab/opengraph', :layout => false,
           :locals => {metadata: metadata, redirect_uri: tab.tab_url({id: unique_id})}

  end
end
