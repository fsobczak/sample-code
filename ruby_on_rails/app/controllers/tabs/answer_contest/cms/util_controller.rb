class Tabs::AnswerContest::Cms::UtilController < ApplicationController
  def export_answers
    answers = @co.tab.answers

    respond_to do |format|
      format.xls do
        send_data answers.to_xls(
                      :headers => ['Name', 'Email', 'User FBID', 'User link', 'Created at', 'Answer URL', 'Votes', 'Likes', 'Description'],
                      :columns => [{:tab_user => {:user => [:name, :email, :fbid, :fb_link]}}, :created_at, :opengraph_url,
                                   :votes_count, :likes_count, :description]
                  )
      end
    end
  end

  def export_users

    tab_users = @co.tab.tab_users

    respond_to do |format|
      #format.csv do
      #  csv_string = CSV.generate do |csv|
      #    csv << ['Name', 'Email', 'Mailing permission', 'Created at', 'Gender', 'Verified',]
      #    tab_users.each do |tu|
      #      if tu.user
      #        csv << [tu.user.name, tu.user.email, tu.mailing_permission, tu.created_at, tu.user.gender, tu.user.verified]
      #      end
      #    end
      #  end
      #  render :text => csv_string
      #end
      format.xls do
        send_data tab_users.to_xls(
                      :headers => ['Name', 'Email', 'FBID', 'Mailing permission', 'Created at', 'Gender', 'Verified'],
                      :columns => [{:user => [:name, :email, :fbid]}, :mailing_permission, :created_at, {:user => [:gender, :verified]}]
                  )
      end
    end
  end
end