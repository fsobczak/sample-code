# encoding: UTF-8
class Tabs::AnswerContest::AjaxController < ApplicationController
  rescue_from FbGraph::InvalidToken, :with => :invalid_token

  def invalid_token
    @user.tab_data(@tab.id)['access_token'] = nil
    @user.save!
    render :json => {:authenticate => true}
  end

  def toggle_like
    raise 'not authorized? wtf?' unless @user
    answer = @co.tab.answers.detect { |a| a._id.to_s == params[:id] }
    ret = {'ok' => true}
    like = Tabs::AnswerContest::Like.where(:answer => answer, :tab_user => @user.tab_user).first
    if like
      like.delete
      ret['liked'] = false
      answer.likes_count -= 1
      answer.save!
    else
      ret['liked'] = true
      like = Tabs::AnswerContest::Like.new
      like.tab_user = @user.tab_user
      #raise like.user_id.inspect
      like.answer = answer
      like.tab = @co.tab
      like.save!

      @user.tab_user.likes << like
      @user.tab_user.save!

      answer.likes << like
      answer.likes_count += 1
      answer.save!
    end

    ret['likes_count'] = answer.likes_count
    render :json => ret
  end


  def vote
    raise 'not authorized? wtf?' unless @user
    answer = @co.tab.answers.detect { |a| a._id.to_s == params[:id] }
    if Tabs::AnswerContest::Vote.where(:answer => answer,
                                       :tab_user => @user.tab_user,
                                       :created_at => (Date.today)..(Date.today+1)).count > 0
      ret = {
          'voted' => true,
          'modal_msg' => {
              header: (t 'answer_contest.already_voted.header'),
              body: (t 'answer_contest.already_voted.body')
          }
      }
    else
      vote = Tabs::AnswerContest::Vote.new
      vote.tab_user = @user.tab_user
      vote.answer = answer
      vote.tab = @co.tab
      vote.remote_ip = request.remote_ip
      vote.save!
      answer.votes << vote
      answer.votes_count += 1
      answer.save!
      ret = {'ok' => true}
    end
    ret['votes_count'] = answer.votes_count
    render :json => ret
  end

  def preview_youtube
    client = YouTubeIt::Client.new
    v = client.video_by(params[:url])
    ret = {}
    if v.title
      ret = {title: v.title, thumbnail_url: "https://i.ytimg.com/vi/#{v.unique_id}/hqdefault.jpg"}
    else
      ret[:error] = (t 'answer_contest.errors.illegal_url')
    end
    render :json => ret
  end

  def add
    @tab = @co.tab
    ret = {'ok' => true}
    u = @user
    #u = GUser.find_or_create # auth powinnismy tutaj juz miec
    if request.method == 'POST'
      raise 'should not happen' unless u.tab_user.action_allowed?
      if params[:mailing_permission]
        u.tab_user.mailing_permission = true
        u.save!
      end
      case params[:selected_class]
        when 'text'
          answer = Tabs::AnswerContest::Text.new({tab_user: u.tab_user, description: params[:text]})
        when 'image'
          answer = Tabs::AnswerContest::Image.new({tab_user: u.tab_user, description: params[:text],
                                                   image_url: params[:url],
                                                   thumb_url: params[:thumb_url],
                                                   original_url: params[:original_url]})
        when 'video'
          answer = Tabs::AnswerContest::Video.new({tab_user: u.tab_user, description: params[:text]})
          answer.fetch_from_url(params[:url])
        else
          raise 'unknown answer type'
      end
      answer.explicitly_shared = params[:explicitly_shared]
      @tab.answers.push(answer)
      @tab.save!
      ret['answer'] = answer
    else # GET
      ret[:action_allowed] = u.tab_user.action_allowed?
      if ret[:action_allowed]
        ret['html'] = render_to_string :partial => 'add'
      else
        ret['html'] = render_to_string :partial => 'add_denied'
      end
    end
    render :json => ret
  end

  def upload
    uploaded_io = params[:files][0]
    uploader = Tabs::AnswerContest::ImageUploader.new(@tab)
    begin
      uploader.store!(uploaded_io)
    rescue CarrierWave::IntegrityError => e
      render :json => {error: e.message} and return
    end
    #$uploadDir = Web::RootDir . "/uploads/app/" . $this->_id . '/';
    render :json => [{url: uploader.url, thumb_url: uploader.thumb.url, original: uploader.original.url}]
  end

end
