class Tabs::AnswerContest::ApiController < GMainController

  def answers
    render :json => @co.tab.answers
  end

  def tab_user
    render :json => @user.tab_user
  end

  def export_users
  end

end