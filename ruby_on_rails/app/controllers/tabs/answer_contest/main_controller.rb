class Tabs::AnswerContest::MainController < GMainController
  layout 'tabs/answer_contest'

  def index
  end

  def start
    render :partial => 'start'
  end

  def answers
    @co.tab.freshen_instagram if @co.tab.answer_classes.include? 'instagram'
    render :partial => 'answers'
  end

  def subpage
    #raise params[:id]
    subpage = @co.tab.res['partials']['subpages'][params[:id].to_i]
    render :partial => 'subpage', :locals => {:subpage => subpage}
  end

  def view
    #raise params[:id]
    obj = @co.tab.answers.select { |a| a.id.to_s == params[:id] }[0]
    render :partial => 'view', :locals => {obj: obj}
  end

  def opengraph
    answer = @co.tab.answers.select { |a| a.id.to_s == params[:answer_id] }[0]

    if answer
      l = {
          redirect_url: answer.tab_url,
          metadata: answer.og_metadata.merge(
              'fb:app_id' => @co.tab.global_params[:fb][:id],
              'fb:explicitly_shared' => answer.explicitly_shared
          )
      }
      render :partial => 'helper/opengraph', :locals => l
    else
      render :partial => 'helper/redirect', :locals => {redirect_url: @co.tab.tab_url}
    end
  end

end
