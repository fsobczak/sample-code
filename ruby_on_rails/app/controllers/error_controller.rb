class ErrorController < ActionController::Base
  layout 'admin'

  def javascript
    raise 'javascript error!'
    render :nothing => true
  end

end