class EditTabController < ApplicationController
  layout 'edit_tab'
  def index
    render :nothing => true, layout: true
  end

  def edit
    @tab_type = params[:tab_type]
    @tab_class = Tabs.const_get(@tab_type.classify)
    render :partial => 'index'
  end

  def save_logic
      response = @co.tab.generic_save_logic(params[:path], params[:data])
      render :json => response
  end

  def delete_subpage
    subpages = @co.tab.res['partials']['subpages']
    subpages.delete_at(params[:k].to_i)
    #raise @co.tab.res['partials']['subpages'].inspect
    @co.tab.save!
    render :json => {:ok => true}
  end

  def upload
    uploaded_io = params[:files][0]
    uploader = FileUploader.new
    uploader.store!(uploaded_io)
    render :json => [{url: uploader.url}]
  end

  def upload_image
    uploaded_io = params[:files][0]
    uploader = ImageUploader.new(params)
    uploader.store!(uploaded_io)
    render :json => [{url: uploader.url}]
  end

  def partial_widget
    #raise params[:res_path].inspect
    render :partial => 'wa/partial', :locals => {:id => params[:res_path]}
  end

  def add_partial_editor
    subpages = @co.tab.res['partials']['subpages']
    k = subpages.length
    subpages.push(Partial.create({type: 'editor', name: "Tab #{k}"}))
    @co.tab.save!
    render :text => "<partial-editor data-k='#{k}' res-path='partials.subpages.#{k}' group-name='subpage'></partial-editor>"
  end
end
