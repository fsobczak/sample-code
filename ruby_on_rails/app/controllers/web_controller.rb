class WebController < ApplicationController

  layout :nil

  caches_page :channel

  def channel
    render :text => '<script src="//connect.facebook.net/en_US/all.js"></script>'
  end

  def ng_templates
    render 'admin/ng_templates', :layout => false
  end

  def index
    render 'start', :layout => 'web'
  end

  def start
  end

  def apps
  end

  def faq
  end

  def nerds
  end

  def terms
  end

  def privacy
  end

  def none
    render :nothing => true
  end

  def pricing
  end

  def register
  end


  def test
    raise request.remote_ip
    #raise 'except ts!'

  end

  def translation
    respond_to do |f|
      f.js { render 'web/translation', :layout => false }
    end
  end
end
