class ApplicationController < ActionController::Base
  protect_from_forgery

  helper AdminHelper
  attr_accessor :co, :fb, :tab, :admin, :tab_type
  attr_accessor :user

  has_mobile_fu false

  before_filter :init_core
  after_filter :set_csrf_cookie_for_ng

  before_filter :set_cache_buster

  rescue_from Exceptions::TabNotFound, :with => :tab_not_found
  rescue_from Exceptions::Authentication, :with => :authentication_failed
  rescue_from FbGraph::InvalidSession, :with => :auth_failed


  def authentication_failed
    render 'system_tab/authentication_failed', :layout => 'system_tab'
  end

  def tab_not_found
    render 'system_tab/not_found', :layout => 'system_tab'
  end

  def is_mobile?
    @mobile ||= is_mobile_device? || is_tablet_device?
  end

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  private
  def init_fb(role, tab_type)
    if role == Core::TAB
      @fb = Fb.new(tab_type, params, session)
    else # WEB, ADMIN, PAGE_ADMIN, TAB_ADMIN, ALIEN
      if params[:code]
        @fb = Fb.new('admin', params, session)
        @admin = Admin.find_by_access_token(@fb.fb_auth.access_token)
        if @admin
          session[:admin_id] = @admin.id
          redirect_to session[:redirect_uri].gsub(/&code=.*/, '') and return
        else
          redirect_to 'https://socialinvaders.com/pl/register' and return
        end
      elsif session[:admin_id]
        @admin = Admin::find(session[:admin_id])
        if @admin
          @fb = Fb.new('admin', :access_token => @admin.access_token)
          @admin = nil if @admin.access_token.nil?
        end
      end
      if role != Core::WEB
        @fb = Fb.new('admin')
        if not @admin and params[:action] != 'register'
          session[:admin_id] = nil
          auth_redirect
        end
      end
    end
  rescue Rack::OAuth2::Client::Error => e
    raise e.response
  end

  private
  def init_core

    init_locale
    case params[:controller]
      when /^tabs\/main/
        role = Core::WEB
      when 'admin', 'page', /^api/, /cms/, /stats/, 'edit_tab'
        role = Core::ADMIN
      when /^tabs/ # it might be TAB_ADMIN
        role = Core::TAB
      when 'web'
        role = Core::WEB

      when /^api\/tabs/, 'tab_admin'
        role = Core::TAB_ADMIN
      when /^alien/
        role = Core::ALIEN
      else
        role = Core::WEB # radmin
    end
    if session[:user_id]
      @user = User.find(session[:user_id])
    end
    if session[:admin_id]
      @admin = Admin::find(session[:admin_id])
    end

    case role
      when Core::WEB, Core::ADMIN, Core::PAGE_ADMIN, Core::ALIEN, Core::TAB_ADMIN
        init_fb(role, 'admin')
        return if performed? # jest redirect - auth na code
        @co = Core.new(role, 'admin', params, request, :admin => @admin)
      when Core::TAB
        @tab_type = params[:controller].split('/')[1]
        init_fb(role, @tab_type)
        @co = Core.new(role, @tab_type, params, request, :fb => @fb)
        if @fb.fb_auth.authorized? and !@user # nowy user
          begin
            @user = User.find_or_create(@fb.fb_auth.access_token, @co.tab, {remote_ip: request.remote_ip})
            session[:user_id] = @user.id
          rescue NameError => e
            raise e # może być auth, bez usera?
          end
        end
        if @user
          @user.tab_user(@co.tab, true)
        end
        I18n.locale = @co.tab.locale.to_sym if @co.tab
      else
        raise 'unknown role'
    end
    @tab = @co.tab

    if !Rails.env.production? && !@co.alien_ip? && session[:dev_cookie] != 'xyz'
      if params[:dev_cookie] == 'xyz' && !session[:dev_cookie]
        session[:dev_cookie] = 'xyz'
        return
      end
      raise "Unauthorized access. IP address: #{request.remote_ip}"
    end
  end

  def auth_failed
    if @co.role != Core::TAB && @co.role != Core::WEB
      #raise 'wt'
      auth_redirect
    end
  end

  def auth_redirect
    client = @fb.fb_auth.client

    uri = "#{request.protocol}#{request.host_with_port}#{request.fullpath}"
    #if Rails.env.production? && request.host_with_port != 'social-invaders.com' #redirect from main domain
    #  redirect_to "#{request.protocol}social-invaders.com#{request.fullpath}" and return
    #end
    client.redirect_uri = uri
    session[:redirect_uri] = uri

    redirect_to client.authorization_uri(:scope => [:email, :manage_pages])
  end

  def init_locale
    if params[:locale]
      I18n.locale = params[:locale].to_sym
      if params[:save_locale]
        session[:locale] = I18n.locale
      end
    elsif session[:locale]
      I18n.locale = session[:locale]
    else
      if request.env['HTTP_ACCEPT_LANGUAGE']
        I18n.locale = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first.to_sym
      end
    end
    if I18n.locale != :pl && I18n.locale != :en
      I18n.locale = :en
    end
    #if params[:controller] =~ /rails_admin/
    #  I18n.locale = 'en'
    #end
    if defined?(RailsAdmin) && self.kind_of?(RailsAdmin::ApplicationController)
      I18n.locale = :en
    end
  end

  def set_csrf_cookie_for_ng
    cookies['XSRF-TOKEN'] = form_authenticity_token if protect_against_forgery?
  end

  protected
  def verified_request?
    super || form_authenticity_token == request.headers['X_XSRF_TOKEN']
  end


end
