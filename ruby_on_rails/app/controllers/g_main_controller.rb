class GMainController < ApplicationController
  layout "tab"
  before_filter :my_init, :check_todo

  def my_init
    if !Rails.env.production? || params[:genless]
      @co.tab.design.generate_less
    end
  end

  def check_todo
    if @co.tab.todo_list
      render 'system_tab/not_ready', :layout => 'system_tab'
    end
  end
end
