class GAdminController < ApplicationController
  def check_xhr
    if params[:action] == 'logout'
      reset_session
      redirect_to '/'
    elsif params[:action] == 'remove_me'
      @co.admin.remove_me
      render :nothing => true
    else
      render :nothing => true, :layout => true unless request.xhr?
    end
  end

end