class PageController < GAdminController

  def show
  end

  def tabs
  end

  def contests
  end

  def add_tab
    render 'web/apps'
  end

end