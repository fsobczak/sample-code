class Order
  include Mongoid::Document

  embedded_in :plan
  embeds_one :invoice_data
  accepts_nested_attributes_for :invoice_data

  field :created_at, type: Time, default: -> { Time.now }
  field :page_fbid # self.plan.page.fbid
  field :paid_from, type: Time
  field :paid_until, type: Time # +1 month
  field :plan, type: String

  field :is_active, default: false
  field :source, type: Hash # {type:'trial'|'code'|'paid', channel: 'dotpay'|'paypal'|'mastercard' }

  field :code # to prefillowane kodem - jest też ustawiony plan, jakoś czas trzeba ustawić?

  def is_active?
    self.is_active
  end


  def self.create_order(values)
    order = self.create(fields)
    unless order.paid_until
      case source['type']
        when 'trial'
          order.paid_until = order.paid_from + 14.days
        when 'paid'
          order.paid_until = order.paid_from + 1.month
      end
    end
  end

end