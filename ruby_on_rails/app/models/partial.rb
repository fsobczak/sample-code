class Partial

  SCROLL_NONE = 'none'
  SCROLL_VERTICAL = 'vertical'
  SCROLL_AUTO = 'auto'

  attr_accessor :type, :state # unused/dismissed/used
  attr_accessor :html, :iframe, :dim
  attr_accessor :url, :name

  def initialize(document)
    assign_attributes(document)
  end

  def assign_attributes(new_attributes, options = {})
    return if new_attributes.blank?

    attributes = new_attributes.stringify_keys
    nested_parameter_attributes = []

    attributes.each do |k, v|
      if respond_to?("#{k}=")
        if v.is_a?(Hash)
          nested_parameter_attributes << [k, v]
        else
          send("#{k}=", v)
        end
      else
        raise "unknown attribute: #{k}"
      end
    end

    # assign any deferred nested attributes after the base attributes have been set
    nested_parameter_attributes.each do |k, v|
      send("#{k}=", v)
    end
  end

  def on_create
    @url = ''
    @iframe = {'height' => 800, 'url' => '', 'scroll' => 0}
    #@html = '<p style="text-align: center;"></p>'
  end

  def to_array
    {type: @type, state: @state,
    html: @html, iframe: @iframe, dim: @dim,
    url: @url, name: @name}
  end

  def self.create(document)
    ret = Partial.new(document)
    ret.on_create
    return ret.to_array
  end

  def save_logic(d)
    d.each do |key, val|
      case key
        when 'name'
          @name = val
        when 'type'
          @type = val
        when 'html', 'editor'
          @html = val
        when 'iframe'
          @iframe.merge!(val)
          if @iframe['url']
            unless @iframe['url'] =~ /http/
              @iframe['url'] = 'http://' + @iframe['url']
            end

          #  C::$response['error'] = "Adres URL nie może być pusty";
          #  } else if (!preg_match('/https?:\/\//', $url)) {
          #    $url = 'http://' . $url;
          #C::$response['replace'] = $url;
          end
      end
    end
  end


end


