class Resource
  include Mongoid::Document

  HEADER_DIM = {'rich' => [780, 200], 'frame' => [810, 300], 'minimal' => [810, 200]}
  OG_IMAGE_DIM = [300, 300]
  UNUSED = 'unused'
  USED = 'used'
  DISMISSED = 'dismissed'
  TEMPLATED = 'templated'

  def self.get(path)
    co = RequestStore.store[:co]
    tmp = co.tab.res
    arr = path.split('.')
    #raise tmp.inspect
    #raise tmp['partials'].inspect
    for k in 0 .. arr.size-1 do
      index = tmp.is_a?(Hash) ? arr[k] : arr[k].to_i
      tmp[index] = {} unless tmp[index]
      tmp = tmp[index]
    end
    tmp
  end

  def self.get_or_create(tab, path, type)
    if type != :file
      raise 'method is depracated'
    end
    tmp = tab.res
    path = path.split('.')
    for k in 0 .. path.size-2 do
      unless tmp[path[k]]
        tmp[path[k]] = {}
      end
      tmp = tmp[path[k]]
    end
    if tmp[path.last]
      tmp[path.last]
    else
      tmp[path.last] = ArrayData.proto(type)
      tab.save!
    end
    tmp[path.last]
  end

  def self.create_image(arr)
    arr['type'] = 'image'
    arr['state'] = UNUSED
    arr['url'] = ''
    arr
  end

  def self.create_resources(tab)
    {
        :partials => partials_list(tab.tab_type),
        :design => {},
        :images => images_list(tab)
    }
  end

  def self.alltabs
    Tab.all.each do |t|
      self.create_for_frame(t)
    end
  end

  def self.create_for_frame(tab)
    tab_type = tab.tab_type
    if %w(answer_contest memo_contest).include? tab_type
      tab.res['partials']['start'] = Partial.create({'type' => 'editor', 'name' => I18n.t("partials.start.#{tab_type}.label"),
                                                     'html' => I18n.t("partials.start.#{tab_type}.html")})
      tab.save!
    end
  end

  private
  def self.images_list(tab)
    ret = {
        :header => Resource.create_image({'dim' => Resource::HEADER_DIM[tab.design.layout['type']]}),
        :og_image => Resource.create_image({'dim' => Resource::OG_IMAGE_DIM})
    }
    case tab.tab_type
      when 'memo_contest'
        cards = {}
        dim = [100, 100]
        10.times do |i|
          cards[i.to_s] = Resource.create_image({'dim' => dim})
        end
        ret['cards'] = cards
        ret['cards_reverse'] = Resource.create_image({'dim' => dim})

      when 'contact'
        dim = [430, 430]
        ret['left'] = Resource.create_image({'dim' => dim})
      else
    end
    ret
  end

  def self.partials_list(tab_type)
    ret = {'fangate' => Partial.create({'type' => 'image'})}
    case tab_type
      when 'static'
        ret['content'] = Partial.create({'type' => 'image'})
      when 'answer_contest', 'memo_contest'
        ret['start'] = Partial.create({'type' => 'editor', 'name' => I18n.t("partials.start.#{tab_type}.label"),
                                       'html' => I18n.t("partials.start.#{tab_type}.html")})
        ret['subpages'] = [
            Partial.create({'type' => 'editor', 'name' => I18n.t("partials.rules.label"),
                            'html' => I18n.t('partials.rules.html')}),
            Partial.create({'type' => 'editor', 'name' => I18n.t("partials.prizes.label"),
                            'html' => I18n.t('partials.prizes.html')})
        ]
      else
    end
    ret
  end

  def self.save_logic(res, data)
    data.each do |type, list|
      case type
        when 'partials'
          partials = res['partials']
          list.each do |id, val|
            if id == 'subpages'
              val.each do |nr, sp|
                part = Partial.new(partials['subpages'][nr.to_i])
                part.save_logic(sp)
                partials['subpages'][nr.to_i] = part.to_array
              end
            else
              part = Partial.new(partials[id])
              part.save_logic(val)
              partials[id] = part.to_array
            end
          end
        else
          res.deep_merge!(data)
      end
    end
  end

end