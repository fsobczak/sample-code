class InvoiceData
  include Mongoid::Document

  embedded_in :invoicable, polymorphic: true

  field :nip
  field :name
  field :street
  field :city
  field :postal
  field :country

end