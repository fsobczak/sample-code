class Core

  WEB = 0
  TAB = 1
  TAB_ADMIN = 2
  ADMIN = 3
  PAGE_ADMIN = 4
  ALIEN = 30

  attr_accessor :role, :sess, :tab_type
  attr_accessor :page, :tab

  attr_accessor :request

  attr_accessor :admin, :preview
  attr_accessor :first_request, :sample, :response


  def tab_admin
    return true if @preview
    fb = RequestStore.store[:fb]
    return true if fb.data['page']['admin']
    false
  end

  def initialize(role, tab_type, params, request, opts = {})
    @admin = opts[:admin]
    @request = request

    @preview = params[:preview]
    @role = role
    @tab_type = tab_type

    case role
      when WEB
      when ADMIN
        init_admin(params)
      when TAB_ADMIN
        init_tab_admin(params)
      when TAB
        init_tab(tab_type, params, opts[:fb])
      when ALIEN
        raise 'unauthorized' unless self.alien?
      else
        raise 'no role!!!'
    end


    RequestStore.store[:co] = self
  end

  def admin_js_vars(params)
    require 'json'
    ret = {
        :locale => I18n.locale,
        :action => params[:action],
    }
    if @admin
      ret.merge!(
          :intercomSettings => {
              'app_id' => 'j8o5q6sa',
              'email' => @admin.email,
              'created_at' => @admin.created_at.to_i,
              'name' => @admin.name
          },
          :admin => {:fbid => @admin.fbid, :name => @admin.name}
      )
    end
    if @page
      ret[:page_fbid] = @page.fbid
      ret[:page_access_token] = @page.access_token
    end
    if @tab
      ret[:tab_id] = @tab.id
      ret[:tab_type] = @tab.tab_type
      #model = Hash[@tab.attributes]
      #raise model.inspect
      #model.delete("videos")
      #ret[:tab_model] = @tab
    end
    if params[:controller] == 'edit_tab'
      ret[:fonts] = Looks::StyleFactory::GOOGLE_FONTS + Looks::StyleFactory::WEB_FONTS
      ret[:background_patterns] = Looks::BgPattern.list
    end
    # TODO akcje z feeds - edit, fonts
    ret.to_json
  end

  def alien?
    (@admin && @admin.alien?)
  end

  def alien_ip?
    return true if !Rails.env.production?
  end

  private
  def init_tab(tab_type, params, fb)
    if params[:tab_id]
      self.tab = Tab.find(params[:tab_id])
    elsif params[:signed_request]
      page_fbid = fb.data['page']['id']
      page = Page.where(fbid: fb.data['page']['id']).first
      self.tab = Tab.where(page_id: page.id, tab_type: tab_type).first
      #raise page_fbid #.inspect
    end
    raise Exceptions::TabNotFound unless self.tab
    self.page = self.tab.page
  end

  def init_admin(params)
    if params[:tab_id]
      self.tab = Tab.find(params[:tab_id])
      self.page = self.tab.page
      if self.page
        raise Exceptions::Authentication unless self.page.authorized(@admin)
      end
    elsif params[:page_fbid] || params[:page_id]
      id = params[:page_fbid] || params[:page_id]
      raise Exceptions::PageNotFound if id.nil?
      if id.length == 24
        @page = Page.find(id)
      else
        @page = Page.where(fbid: id).first
      end
      return if params[:action] == 'add_page'
      raise Exceptions::PageNotFound unless self.page
      raise Exceptions::Authentication unless self.page.authorized(@admin)
    end
  end
end
