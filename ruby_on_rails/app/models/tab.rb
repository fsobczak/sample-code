class Tab
  include Mongoid::Document

  def self.init_radmin
    if defined? RailsAdmin
      rails_admin do
        list do
          field :created_at
          field :type_name
          field :universal_link do
            pretty_value do
              %{<a href='#{value}'>#{value}</a>}.html_safe
            end
          end
          field :page do
            pretty_value do
              %{<a href='#{value.link}'>#{value.name}</a>}.html_safe if value
            end
          end
        end
      end
    end
  end

  self.init_radmin


  embeds_one :design, :class_name => "Looks::Design"
  embeds_one :setting
  embeds_one :contest, :class_name => "Contest"
  embeds_one :stats, :class_name => "Stats"

  belongs_to :page

  has_many :tab_users, :class_name => "GTabUser"

  accepts_nested_attributes_for :design
  accepts_nested_attributes_for :setting
  accepts_nested_attributes_for :contest
  accepts_nested_attributes_for :stats


  field :created_at, default: -> { Time.now }

  field :tab_type, type: String
  field :tab_slot, type: Integer
  field :page_fbid, type: String
  field :page_id, type: String

  field :mailing, type: Hash, default: {enabled: 'false', label: 'Zapisz mnie na newsletter', required: 'false'}
  field :universal_link
  field :locale, default: -> { I18n.locale.to_s }

  field :og_title, type: String
  field :og_description, type: String

  #index({page_id: 1}, {unique: true, name: "page_id_index"})
  #index({page_fbid: 1}, {unique: true, name: "page_fbid_index"})

  field :fangate_state, type: String
  field :res, type: Hash


  def on_create(opts = nil)
    self.fangate_state = self.class.is_contest? ? 'popup' : 'off'
    self.res = Resource.create_resources(self)
    self.design = Looks::Design.new
    self.design.on_create

    #Looks::Template.fill_with_default_design(self)
    # to musi byc po design.on_create
    # generalnie chyba init wczesniej jest zbedny, ale co tam:)

    self.setting = Setting.new.on_create

    self.stats = Stats.new.on_create

    if (global_params[:flags] & GlobalTabParams::CONTEST) != 0
      self.contest = Contest.new.on_create
    end

    client = Bitly.client
    #link = "https://socialinvaders.com/l/#{self._id}"
    self.universal_link = client.shorten(local_link).short_url
    self
  end

  def local_link
    "https://socialinvaders.com/l/#{self._id}"
  end

  def full_tab_type
    self.tab_type
  end

  def opengraph_meta
    {'og:description' => self.og_description, 'og:title' => self.og_title,
     'og:image' => 'https://socialinvaders.com' + self.res['images']['og_image']['url'],
     'og:url' => self.local_link, 'og:type' => 'article'}
  end

  # this is targeted link (preview / mobile / desktop)s
  def tab_url(app_data = nil, on_facebook = true, device = 'desktop')
    if on_facebook
      ret = self.page.link + '?sk=app_' + self.global_params[:fb][:id]
    else
      ret = WebInfo.host + "/tabs/#{self.tab_type}/main?tab_id=#{self.id}&preview=liked&device=#{device}"
    end
    ret += "&app_data=#{app_data}" if app_data
    #ret += '&app_data='+URI.escape(app_data.to_json) if app_data
    ret
  end

  def self.create_tab(tab_type, tab_slot, opts = {})
    tab = nil
    begin
      cls = Object.const_get('Tabs').const_get(tab_type.classify)
      tab = cls.new
      tab.tab_type = tab_type
      tab.tab_slot = tab_slot
      if opts[:page]
        tab.page_id = opts[:page].id
        tab.page_fbid = opts[:page].fbid
      end
      tab.design = Looks::Design.new(layout: tab.global_params[:default_layout])

      tab.on_create(opts)
      tab.save!
        #raise 'ojoj'
    rescue => ex
      tab.delete if tab
      raise ex
    end
    tab
  end

  def create_feed(scenario, opts = {})
    self.options.create_feed(scenario)
  end

  def draw_fangate(co, fb)
    if co.preview == 'not_liked' || (!co.preview && !fb.page_liked)
      self.fangate_state != 'off' ? self.fangate_state : false
    else
      false
    end
  end

  def get_steps
    #return STATIC STEPS static::$steps
  end

  def has_cms?
    #raise global_params[:flags].inspect
    global_params[:flags] & GlobalTabParams::CMS != 0
  end

  def self.is_contest?
    #raise self.name.demodulize.underscore
    GlobalTabParams::PARAMS[self.name.demodulize.underscore][:flags] & GlobalTabParams::CONTEST != 0
    #global_params[:flags]
  end


  def global_params
    ret = GlobalTabParams.params(tab_type, tab_slot)
    #raise ret.inspect
  end

  def generic_save_logic(path, json_data)
    reversed_path = path.split('.').reverse
    data = json_data
    raise Exceptions::InvalidSave.new('invalid path, no tab.') unless path.starts_with? 'tab'
    reversed_path.each do |p|
      break if p == 'tab'
      data = {p => data}
    end
    response = {}
    begin
      data.each do |k, v|
        case k
          when 'design'
            self.design.save_logic(v)
          when 'contest'
            self.contest.save_logic(v)
          when 'res'
            Resource.save_logic(self.res, v)
          when 'fangate_state'
            self.send("#{k}=", v)
          when 'setting'
            self.setting.save_logic(v)
          when 'mailing'
            self.mailing.merge!(v)
          else
            if self.respond_to?("#{k}=")
              self.send("#{k}=", v)
            else
              raise Exceptions::InvalidSave.new('invalid path, value not recognized') unless specific_save_logic({k => v})
            end
        end
      end
      response[:ok] = true
    rescue Exceptions::InvalidSave => ex
      response[:error] = ex.message
    rescue Exceptions::WarningSave => ex
      response[:ok] = true
      response[:info] = ex.message
    end
    save!
    response
  end

  def js_vars(co, fb)
    tab_vars = {
        tab_id: self.id,
        tab_type: self.tab_type,
        universal_link: self.universal_link,
        tab_url: self.tab_url,
        tab_slot: self.tab_slot,
        path: fb.uri_path,
        invite: self.setting.invite
    }
    if self.page
      tab_vars['page_fbid'] = self.page.fbid
    end
    if self.class.is_contest?
      tab_vars['contest'] = self.contest.attributes.merge!({status: self.contest.status})
    end
    tab_vars['authorized'] = fb.fb_auth.authorized?
    if fb.data
      tab_vars['sr_data'] = fb.data
    end


    #if ($this->_jsVars)
    #  {
    #      $appVars = array_merge($appVars, $this->_jsVars);
    #  }

    tab_vars
  end

  def page
    Page.find(self.page_id)
  end


  def type_name
    I18n.t "tab.#{self.tab_type}.name"
  end


  def edit_url
    "/tabs/#{tab_type}/edit?tab_id=#{self.id}"
  end


  def cms_url
    raise 'no cms url specified?' if has_cms?
  end

  def stats_url
    "/tabs/#{tab_type}/stats?tab_id="+self.id+'&page_fbid='+self.page_fbid
  end

  def as_json(options)
    ret = super(:methods => [:full_tab_type, :global_params, :edit_url, :cms_url, :stats_url, :type_name, :tab_url])
    ret['global_params'][:fb].delete(:secret)
    ret
  end

end