class GTabUser
  include Mongoid::Document
  belongs_to :user
  belongs_to :tab


  field :access_token
  field :mailing_permission, type: Boolean, default: true
  field :created_at, type: Time, default: -> { Time.now }

  def as_json(options)
    super(:except => [:access_token, :mailing_permission], :include => {:user => {:only => User::PUBLIC_VARS}})
  end

end