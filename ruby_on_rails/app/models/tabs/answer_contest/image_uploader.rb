class Tabs::AnswerContest::ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  FULL_WIDTH = 500
  FULL_MAX_HEIGHT = 500

  THUMBNAIL_WIDTH = 228
  THUMBNAIL_MAX_HEIGHT = 400

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  process :resize_to_limit => [FULL_WIDTH, FULL_MAX_HEIGHT]

  version :original do
  end

  version :thumb do
    process :resize_to_fit => [THUMBNAIL_WIDTH,THUMBNAIL_MAX_HEIGHT]
  end

  def store_dir
    co = RequestStore.store[:co]
    "uploads/tabs/#{co.tab.id}/"
  end

  def filename
    if original_filename
      @name ||= Digest::MD5.hexdigest(File.dirname(current_path))
      "#{original_filename}-#{@name}.#{file.extension}"
    end
  end

end