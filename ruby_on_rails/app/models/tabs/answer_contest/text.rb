class Tabs::AnswerContest::Text < Tabs::AnswerContest::Answer
  field :text_only, default: 'dummy'

  def og_metadata
    {
        'og:title' => self.tab.question,
        #'og:image' => self.youtube_video.thumbnail_url('hq'),
        'og:url' => self.opengraph_url,
        'og:type' => 'inv_answercontest:answer'

    }
  end


end