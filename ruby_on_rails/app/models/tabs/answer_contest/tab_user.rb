class Tabs::AnswerContest::TabUser < GTabUser
  #has_many :answers, class_name: 'Tabs::AnswerContest::Answer'
  has_many :likes, class_name: 'Tabs::AnswerContest::Like'

  def on_create
    super
    #tab_data['answers'] = []
  end

  def action_today_count(tab)
    (tab.answers.select { |a| a.user_id == self.id }).count
  end

  def can_vote_on?(answer)
  end

  def votes_today_count(tab)
  end

  def action_allowed?
    true
  end

  def as_json(options = nil)
    super(options).merge(:likes => self.likes.as_json)
  end

end