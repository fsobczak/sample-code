class Tabs::AnswerContest::Image < Tabs::AnswerContest::Answer

  field :image_url
  field :original_url
  field :thumb_url

  def og_metadata
    {
        'og:title' => self.tab.question,
        'og:image' => WebInfo.host + self.image_url,
        'og:url' => self.opengraph_url,
        'og:type' => 'inv_answercontest:answer'
    }
  end

end
