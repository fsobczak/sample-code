class Tabs::AnswerContest::Video < Tabs::AnswerContest::Answer
  embeds_one :youtube_video
  accepts_nested_attributes_for :youtube_video

  def fetch_from_url(url)
    client = YouTubeIt::Client.new
    client_video = client.video_by(url)
    self.youtube_video = YoutubeVideo.create_from_data(client_video)
    self
  end

  #<meta property="fb:app_id" content="193048140809145" />
  #<meta property="og:title" content="Sample Object" />
  #<meta property="og:image" content="https://s-static.ak.fbcdn.net/images/devsite/attachment_blank.png" />
  #<meta property="og:url" content="http://samples.ogp.me/226075010839791" />
  #<meta property="og:type" content="object" />

  def og_metadata
    {
        'og:title' => self.youtube_video.title,
        'og:image' => self.youtube_video.thumbnail_url('hq'),
        'og:url' => self.opengraph_url,
        'og:type' => 'inv_answercontest:answer'
    }
  end

end