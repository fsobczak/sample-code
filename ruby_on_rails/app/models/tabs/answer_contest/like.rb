class Tabs::AnswerContest::Like
  include Mongoid::Document

  field :created_at, type: Time, default: -> { Time.now }
  field :action_fbid # not used atm

  belongs_to :answer, class_name: 'Tabs::AnswerContest::Answer'
  belongs_to :tab_user, class_name: 'Tabs::AnswerContest::TabUser'
  belongs_to :tab, class_name: 'Tabs::AnswerContest'

  def as_json(options = nil)
    super(:except => [ :tab_user_id, :tab_id])
  end


end