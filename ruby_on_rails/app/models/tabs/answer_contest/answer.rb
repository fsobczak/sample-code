class Tabs::AnswerContest::Answer
  include Mongoid::Document
  embedded_in :tab, class_name: 'Tabs::AnswerContest'

  belongs_to :tab_user, class_name: 'Tabs::AnswerContest::TabUser'

  has_many :votes, class_name: 'Tabs::AnswerContest::Vote'
  field :votes_count, type: Integer, default: 0

  has_many :likes, class_name: 'Tabs::AnswerContest::Like'
  field :likes_count, type: Integer, default: 0

  field :created_at, type: Time, default: -> { Time.now }
  field :description, type: String
  field :status
  field :winner
  field :explicitly_shared, default: true

  def tab_url
    tab.tab_url(self.id)
  end

  def opengraph_url
    WebInfo.host + "/tabs/answer_contest/main/opengraph/?tab_id=#{self.tab.id}&answer_id=#{self.id}"
  end


  def as_json(options)
    super(:except => [:votes], :methods => [:opengraph_url]).merge(:tab_user => self.tab_user.as_json)
  end

end