class Tabs::AnswerContest::Vote
  include Mongoid::Document

  field :created_at, type: Time, default: -> { Time.now }
  field :remote_ip, type: String

  belongs_to :answer, class_name: 'Tabs::AnswerContest::Answer'
  belongs_to :tab_user, class_name: 'Tabs::AnswerContest::TabUser'
  belongs_to :tab, class_name: 'Tabs::AnswerContest'
end