class Tabs::AnswerContest::InstagramItem < Tabs::AnswerContest::Answer

  field :instagram_data
  field :added_by_hashtag

  def og_metadata
    {
        'og:title' => self.tab.question,
        'og:image' => WebInfo.host + self.instagram_data['images']['low_resolution']['url'],
        'og:url' => self.opengraph_url,
        'og:type' => 'inv_answercontest:answer'
    }
  end

end
