# encoding: UTF-8
class Tabs::AnswerContest < Tab
  include Mongoid::Document

  THUMBNAIL_WIDTH = 228
  THUMBNAIL_MAX_HEIGHT = 2000

  STEPS = %w(basic design partials viral publish) # advanced design partials viral)
  embeds_many :answers, class_name: "Tabs::AnswerContest::Answer"
  accepts_nested_attributes_for :answers

  field :question, type: String, default: "title / question"
  field :answer_classes
  field :answer_length, type: Integer, default: 300

  field :instagram_hashtag, type: String
  field :instagram_updated_at, default: -> { Time.now - 1.year }

  def freshen_instagram
    self.instagram_updated_at = Time.now - 1.year
    #self.answers.each {|a| a.delete }
    #return
    if self.instagram_updated_at < 20.minutes.ago
      list = Instagram.tag_recent_media('glogow', {})
      list.select! {|item| item.created_time.to_i > self.instagram_updated_at.to_i}
      list.each do |item|
        #raise 'haha!'
        answer = Tabs::AnswerContest::InstagramItem.new({instagram_data: item}) # no tab_user on this one
        answer.created_at = Time.at(item.created_time.to_i)
        answer.added_by_hashtag = self.instagram_hashtag
        #raise answer.created_at.inspect
        self.answers.push(answer)
      end
      self.instagram_updated_at = Time.now
      self.save!
    end
  end

  #def opengraph_meta
  #  {'og:description' => self.description, 'og:title' => self.question}
  #end

  def on_create(opts = nil)
    super
    if opts[:subtype] == 'multi'
      self.answer_classes = ['image', 'text', 'video']
    else
      self.answer_classes = [opts[:subtype]]
    end
  end

  def full_tab_type
    subtype = (self.answer_classes.size == 1) ? self.answer_classes[0] : 'multi'
    "#{self.tab_type}.#{subtype}"
  end

  def type_name
    I18n.t "tab.#{full_tab_type}.name"
  end

  def todo_list
  end

  def specific_save_logic(data)
    data.each do |k, v|
      case k
        when 'answer_length'
          if Util.is_numeric? v
            self.answer_length = v
          else
            raise Exceptions::InvalidSave.new('Wpisana wartosc musi byc liczba')
          end
        else
          if %w(question).include? k
            self.send "#{k}=", v
          else
            return false
          end
      end
    end
    true
  end

  def cms_url
    '/tabs/answer_contest/cms/answers?tab_id='+self.id+'&page_fbid='+self.page_fbid
  end


  def js_vars(co, fb)
    super(co, fb).merge!({answer_classes: self.answer_classes})
  end
end