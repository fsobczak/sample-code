class Tabs::Youtube < Tab
  embeds_many :videos, class_name: "YoutubeVideo"
  accepts_nested_attributes_for :videos

  STEPS = %w(basic design partials publish) # 'viral']

  field :data_source, type: Hash, default: {'type' => 'favorites', 'user' => 'etccorp'}
  field :sorted, default: 'new'
  field :autoplay, type: Boolean, default: false

  def video_opengraph_url(unique_id)
    "#{WebInfo.host}/tabs/youtube/main/opengraph?tab_id=#{self.id}&unique_id=#{unique_id}"
  end

  def todo_list
    if self.videos.empty?
      [{:msg => 'todo.youtube.list_empty'}]
    else
      false
    end
  end

  def specific_save_logic(data)
    data.each do |k, v|

      case k
        when 'data_source'
          self.data_source.merge!(v)
          save!
          self.videos = []; save!
          fetch_data_source
        when 'autoplay'
          self.autoplay = v
        when 'sorted'
          self.sorted = v
          case v
            when 'new'
              self.videos.sort! {|a,b| a.uploaded_at <=> b.uploaded_at }
            when 'views'
              self.videos.sort! {|a,b| a.view_count <=> b.view_count }
            when 'rating'
              self.videos.sort! {|a,b| a.rating <=> b.rating if a.rating and b.rating }
            when 'duration'
              self.videos.sort! {|a,b| a.duration <=> b.duration }
          end
        else
          raise Exceptions::InvalidSave.new('path not recognized')
      end
    end
    true
  end

  def fetch_data_source
    client = YouTubeIt::Client.new
    begin
      case self.data_source['type']
        when 'favorites'
          resp = client.videos_by(:favorites, :user => self.data_source['user'])
        when 'uploads'
          resp = client.videos_by(:user => self.data_source['user'])
      end
    rescue
      raise Exceptions::InvalidSave.new("User doesnt exist")
    end

    raise Exceptions::WarningSave.new("List is empty") if resp.videos.empty?
    videos = resp.videos
    self.videos = videos.collect do |v|
      YoutubeVideo.create_from_data(v)
    end
  end
end