class ArrayData
  FEED_DISABLED = 0
  FEED_ENABLED = 1
  FEED_PERMANENT = 2

  def self.proto(cls, value = {})
    case cls
      when :map
        {'places' => [{'address' => '', 'latlng' => ''}], 'zoom' => 14}
      when :image, :file
        {'state' => Resource::UNUSED, 'url' => ''}
      when :invite
        {'title' => t('defaults.invite.title'), 'message' => 'defaults.invite.message'}
      else
        raise 'no proto type of '+cls.to_s
    end
  end

  def self.ensure_structure
    raise 'to jest uzywane?'
  end

  def save_map(map, value)
    map[:places][0] = value[:places]
    map[:zoom] = value[:zoom]
  end

  GAME_FEEDS = {main: {enabled: FEED_PERMANENT}}
  MAIN_FEEDS = GAME_FEEDS

  def self.feeds_info(tab_type, return_defaults = false)
    case tab_type
      when 'AnswerContest'
        MAIN_FEEDS
      when 'MemoContest'
        GAME_FEEDS
      else
        {}
    end
    if return_defaults
      defaults = {}
      ret.each do |id, arr|
        defaults[id] = {enabled: ret[id]['enabled']}
        %w(name caption description).each do |field|
          defaults[id][field] = t "feeds.#{tab_type}.#{id}.#{field}"
        end
      end
      ret[:defaults] = defaults
    end
  end

  def self.design_proto(tab, item)
    case item
      when 'color'
        return '#44AAFF'
      when 'px'
        return '5px'
      when 'font'
        return 'Open Sans'
      when 'background-image'
        return Looks::BgPattern.default(tab)
    end
  end

  def self.design_items_list(tab)
    tab_type = tab.tab_type
    frame_base = {'main-color' => 'color', 'bg-color' => 'color',
                  'text-font' => 'font', 'menu-font' => 'font',
                  'header-font' => 'font',
                  'background-image' => 'background-image'
    }

    return frame_base #if Util.only_minimal_design(tab_type)
  end
end
