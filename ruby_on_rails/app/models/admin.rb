class Admin
  include Mongoid::Document

  embeds_one :invoice_data, as: :invoicable, autobuild: true
  accepts_nested_attributes_for :invoice_data

  field :access_token, type: String
  field :instagram_access_token, type: String
  field :created_at, type: Time, default: -> { Time.now }

  field :name, type: String
  field :email, type: String
  field :location, type: Hash
  field :fbid, type: String
  field :timezone, type: String
  field :raw_pages, type: Array, default: []
  field :pages, type: Hash, default: {}

  field :remote_ip

  field :mailing_permission, type: Boolean, default: false

  def alien?
    ['589479695'].include? self.fbid
  end

  def tz
    Time.zone
  end

  def remove_me
    u = User.where({fbid: self.fbid}).first
    if u
      u.tab_users.delete_all
      u.delete
    end
    delete
    #raise u.inspect
  end

  def refresh_fb(me = nil, opts = {}) # jezeli bedzie potrzebny nowy
    me ||= FbGraph::User.me(self.access_token).fetch
    self.fbid = me.identifier
    self.name = me.name
    self.email = me.email
    self.location = me.location.raw_attributes if me.location
    self.timezone = me.timezone
    self.remote_ip = opts[:remote_ip] if opts[:remote_ip]
    save!
  end

  def self.create_with_access_token(access_token, opts)
    me = FbGraph::User.me(access_token).fetch
    admin = Admin.where(fbid: me.identifier).first
    if admin
      raise 'admin already exists!'
    else
      admin = Admin.new
      admin.access_token = access_token
      admin.refresh_fb(me, opts)
    end
    admin.save!
    admin

  end

  def self.find_by_access_token(access_token)
    me = FbGraph::User.me(access_token).fetch
    admin = Admin.where(fbid: me.identifier).first
    return nil unless admin
    # raise?
    admin.access_token = access_token
    admin.save!
    admin
  end


  def fetch_pages
    data = FbGraph::Node.new('/me/accounts', :access_token => self.access_token).fetch
    data = data.raw_attributes['data']
    self.raw_pages = data

    data.each do |tmp|
      page = Page.where(fbid: tmp['id']).first
      if page
        page.access_token = tmp['access_token']
        page.save!
        self.pages[tmp['id']] = tmp['name']
      end
    end
    save!
    self.pages
  end

  def as_json(options)
    super(:except => :raw_pages, :methods => :invoice_data)
  end

end