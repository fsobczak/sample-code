class YoutubeVideo
  include Mongoid::Document
  embedded_in :tabs_youtube
  embedded_in :answer, class_name: "Tabs::AnswerContest::Video"

  field :uploaded_at
  field :unique_id
  field :duration
  field :description
  field :title
  field :rating
  field :view_count

  def self.create_from_data(data)
    v = self.new
    v.uploaded_at = data.updated_at
    v.unique_id = data.unique_id
    v.duration = data.duration
    v.description = data.description
    v.title = data.title
    v.view_count = data.view_count
    v.rating = data.rating.average if data.rating
    v
  end

  def thumbnail_url(prefix = '')
    "https://i.ytimg.com/vi/#{self.unique_id}/#{prefix}default.jpg"
  end


end