class Invite
  include Mongoid::Document
  belongs_to :tab_user, class_name: 'GTabUser'
  belongs_to :tab

  field :created_at, type: Time, default: -> {Time.now}
  field :friend_id
  field :request

end