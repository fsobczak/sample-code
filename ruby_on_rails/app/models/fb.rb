require 'rack/oauth2/client/error'

class Fb

  attr_accessor :fb_page, :fb_sr, :fb, :tab_type, :tab_slot, :app_data
  attr_accessor :app_id
  attr_accessor :fb_auth
  attr_accessor :uri_path
  attr_accessor :data

  def page_liked
    self.data['page']['liked']
  end

  def page_admin
    self.data['page']['admin']
  end

  def initialize(tab_type, params = {}, session = {})
    @tab_type = tab_type
    global_params = GlobalTabParams::params(tab_type, params[:tab_slot], :preview => params[:preview])
    @app_id = global_params[:fb][:id]
    @fb_auth = FbGraph::Auth.new(global_params[:fb][:id], global_params[:fb][:secret])

    if params[:signed_request]
      @fb_auth.from_signed_request(params[:signed_request])
      @data = @fb_auth.data
    elsif params[:access_token] # authorize
      @fb_auth.access_token = params[:access_token]

    elsif params[:code] # in Web / Admin and authorize!!
      client = @fb_auth.client
      client.redirect_uri = session[:redirect_uri]
      #raise opts[:redirect_uri]
      client.authorization_code = params[:code]
      access_token = client.access_token! :client_auth_body # => Rack::OAuth2::AccessToken
      @fb_auth.access_token = access_token
    end

    @uri_path = ''

    RequestStore.store[:fb] = self

  rescue Rack::OAuth2::Client::Error => er
    #raise er.status.inspect
    raise er.response.inspect
  end

end