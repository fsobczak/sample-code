class GlobalTabParams
  CMS = 1
  CONTEST = 2
  GAME = 4

  FB_PARAMS = {
      #:dev => [
      #    {id: '320924618032925', secret: '477f373d4ff5845b707afaf9ed5038bd',
      #     ns: 'invaders_dev'}
      #],
      :admin => [
          {:id => '438616219559149', :secret => '553c490e6f44a8546e01ba86004be531', :ns => 'socialinvaders'}
      ],
      :static => [
          {:id => '221378334627250', :secret => '043fb32757d9b2460ee719f3286778ee', :ns => 'inv_static'},
      #{:id => '242947702470580', :secret => 'ca78f5e5481676bdcc09ddc6f3245806', :ns => 'inv_static_b'}
      ],

      :map => [
          {:id => '320873331312782', :secret => '959f585c5673fa9990e779554bdc86d2', :ns => 'inv_maps'}
      ],
      :instagram_tab => [
          {:id => '320873331312782', :secret => '959f585c5673fa9990e779554bdc86d2', :ns => 'inv_instagram'}
      ],
      # akcje: liked video
      :youtube => [
          {:id => '370950132928871', :secret => '3d9027478fc0e67188092c6da4c39e68', :ns => 'inv_youtube'}
      ],

      :contact => [
          {:id => '312407912175958', :secret => '9692f88438b4d47f8ec9c671403cc7d7', :ns => 'inv_contact'}
      ],
      :redirect => [# przepisać!!!... czemu redirect?
          {:id => '221378334627250', :secret => '043fb32757d9b2460ee719f3286778ee', :ns => 'inv_static'},
      ],
      :topfans => [
          {:id => '255426521264413', :secret => '77a7f15ec235d7afa60a1411fe0347a3', :ns => 'inv_topfans'},
      ],
      :answer_contest => [
          {:id => '453477868000148', :secret => 'f2ccf794047b614c3076aa84438af0c9', :ns => 'inv_answercontest'},
      #{:id => '120256428123836', :secret => '0cdf60fb295d4393ec41df6ffbf80f22', :ns => 'inv_answercontest_b'}
      ],
      :memo_contest => [
          {:id => '393460800726370', :secret => 'bacf8a03c9f52adfa4fe573dcd6a9f88', :ns => 'inv_memo'},
      ],
      :coupons => [
          {:id => '512179488854524', :secret => '41819ead8c3d6bf759b734caef92eb26', :ns => 'inv_coupons'},
      ]
  }


  PARAMS = {
      dev: {:fb => :dev},
      'admin' => {:fb => :admin},

      'youtube' => {:fb => :youtube, :flags => 0},
      'redirect' => {fb: :redirect, flags: 0},
      'map' => {fb: :map, flags: 0},
      'topfans' => {fb: :topfans, flags: 0},
      'instagram_tab' => {fb: :instagram_tab, flags: 0},
      'contact' => {:fb => :contact, :flags => 0, },
      'coupons' => {:fb => :coupons,
                    :flags => 0},
      'static' => {:fb => :static, :flags => 0},
      'twitter' => {:fb => :twitter, :flags => 0},
      'files' => {:fb => :static, :flags => 0},
      'memo_contest' => {:fb => :memo_contest, :default_layout => {"type" => "frame"},
                         :flags => CONTEST | CMS,
      },
      'quiz_contest' => {:fb => :quiz_contest, :default_layout => {"type" => "frame"},
                         :flags => CONTEST | CMS,
      },
      'answer_contest' => {:fb => :answer_contest, :default_layout => {"type" => "frame"},
                           :flags => CONTEST | CMS,
      },
  }

  attr_accessor :fb

  def self.params(tab_type, tab_slot, opts = {})
    tab_slot = 0 if tab_slot == nil
    ret = PARAMS[tab_type].dup
    if true || Rails.env.production?
      raise ret[:fb].inspect unless FB_PARAMS[ret[:fb]]
      ret[:fb] = FB_PARAMS[ret[:fb]][tab_slot.to_i].dup
    else
      ret[:fb] = FB_PARAMS[:dev][0].dup
    end

    ret[:default_layout] = {"type" => "minimal"} unless ret[:default_layout]
    ret[:tab_type] = tab_type
    ret
  end

  def self.find_free_tab_slot(page, tab_type)
    for k in 0 .. self.tab_slots_count(tab_type)-1
      if Tab.where(page_id: page.id, tab_type: tab_type, tab_slot: k).count == 0
        return k
      end
    end
    return nil
  end

  def self.tab_slots_count(tab_type)
    #raise tab_type.inspect
    #raise PARAMS[tab_type].inspect
    FB_PARAMS[PARAMS[tab_type][:fb]].size
  end

  def self.tabs_list
    PARAMS.select do |key, val|
      key != 'admin' && key != :dev
    end
  end

  def self.admin_app_fbid
    return FB_PARAMS[:admin][0][:id]
  end
end