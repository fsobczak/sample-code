class Util

  def self.create_todo(path)
    {:message => I18n.t('todo.' + path)}
  end

  def self.is_numeric?(obj)
    obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

  def self.is_valid_email?(str)
    str.match(/\b[A-Z0-9._%a-z\-]+@(?:[A-Z0-9a-z\-]+\.)+[A-Za-z]{2,4}\z/) != nil
  end

  def self.path_val(path, tab = nil)
    tab = RequestStore.store[:co].tab unless tab
    arr = path.split('.')
    k = 2
    case arr[1]
      when 'design'
        ret = tab.design
      when 'setting'
        ret = tab.setting
      when 'contest'
        ret = tab.contest
      when 'fangate'
        ret = tab.fangate
      else
        ret = tab
        k = 1
    end
    #raise ret.inspect
    while k < arr.size
      ret = ret[arr[k]] # // nie może być referencja, bo się robi object-box['enabled']
      k += 1
    end
    ret
  end

  def self.google_fonts_link(fonts)
    return '' if fonts.size == 0
    fonts = fonts.collect do |f|
      f.gsub(' ', '+')
    end
    "<link href='https://fonts.googleapis.com/css?family=#{fonts.join('|')}&subset=latin,latin-ext' rel='stylesheet' type='text/css'>"

  end
end
