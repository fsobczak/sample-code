module Select
  YES_NO = {true => 'select_yes', false => 'select_no'}
  ON_OFF = {true => 'select_yes', false => 'select_no'}

  IMAGE_ON_OFF = {1 => 'visible', 2 => 'hidden'}

  CONTACT_LEFT = {
      'none' => 'select_off',
      'map' => 'map',
      'image' => 'image'
  }

  DIRECTION = {
      'bottom' => 'up',
      'bottom left' => 'bottom_left',
      'left' => 'left',
      'top left' => 'top_left',
      'top' => 'top',
      'top right' => 'top_right',
      'right' => 'right',
      'bottom right' => 'bottom_right'
  }
  STATIC = {
      'image' => "image",
      'editor' => "editor",
      'html' => "html",
      'iframe' => "iframe"
  }
  BACKGROUND = {
      solid: 'solid',
      gradient: 'gradient',
      image: 'image'
  }
  SHADOW = {
      'none' => 'none',
      'simple' => 'simple',
      'vertical' => 'vertical',
      'shine' => 'shine',
      'inset' => 'inset',
      'corners' => 'corners'
  }
  FANGATE = {
      'popup' => 'popup',
      'static' => 'static',
      'off' => 'select_off'
  }
  YOUTUBE_DATA = {
      'uploads' => 'uploads',
      'favorites' => 'favorites'
  }
  INSTAGRAM_SOURCE_TYPE = {
      'hashtag' => 'hashtag',
      'user' => 'user'
  }
  YOUTUBE_SORTED = {
      :new => 'sorted_new',
      :views => 'sorted_views',
      :rating => 'sorted_rating',
      :duration => 'sorted_duration'
  }


end