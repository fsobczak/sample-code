class Contest
  include Mongoid::Document
  embedded_in :tab

  UNDEFINED = 'undefined'
  PREPARATION = 'preparation'
  RUNNING = 'running'
  CHOOSING_WINNERS = 'choosingWinners'
  FINISHED = 'finished'

  field :plays, type: Hash, default: {max: 3, free: 1, invites_for_play: 3} # [max / free / invites_for_play]
  field :start, type: Time, default: -> { Time.now }
  field :finish, type: Time, default: -> { Time.now + 14.days }


  def status
    now = Time.now
    if now < self.start
      'preparation'
    elsif now < self.finish
      'running'
    elsif !self.is_finished?
      'choosing_winners'
    else
      'finished'
    end
  end

  def is_running?
    self.start < now < self.finish
  end

  # sitodo do wyboru zwyciezcow
  def is_finished?
    return true
  end

  def on_create
    self
  end

  def save_logic(data)
    data.each do |k, v|
      case k
        when 'plays'
          self.plays.merge!(v)
        when 'time'
          self.start = Time.at(v['start'].to_i)
          self.finish = Time.at(v['finish'].to_i)
      end
    end
    save!
  end

end
