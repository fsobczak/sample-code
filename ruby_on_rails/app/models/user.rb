class User
  include Mongoid::Document

  PUBLIC_VARS = [:name, :gender, :fbid, :_id]
  has_many :tab_users, class_name: 'GTabUser'

  field :created_at, type: Time, default: -> { Time.now }

  field :name
  field :email
  field :fbid, type: String

  field :remote_ip, type: String

  field :verified
  field :gender
  field :location
  field :timezone
  field :updated_time
  field :locale

  def fb_link
    'https://fb.com/profile.php?id='+self.fbid
  end


  def refresh_fb(opts) # jezeli bedzie potrzebny nowy
    me = opts[:me] || FbGraph::User.me(self.tab_data['access_token']).fetch
    self.fbid = me.identifier
    self.name = me.name
    self.email = me.email
    self.gender = me.gender
    self.location = me.location.raw_attributes if me.location
    self.timezone = me.timezone
    self.locale = me.locale
    self.verified = me.verified
    self.updated_time = me.updated_time
    if opts[:remote_ip]
      self.remote_ip = opts[:remote_ip]
    end
    save!
  end

  def self.find_or_create(access_token, tab, opts)
    me = FbGraph::User.me(access_token).fetch
    u = User.where(fbid: me.identifier).first # this failed sometimes, creating duplicates - maybe me.identifier empty?
    unless u
      u = self.new
      u.tab_user(tab, true).access_token = access_token
      u.refresh_fb(:me => me)
    end
    if u.tab_user(tab)
      u.tab_user(tab).access_token = access_token
    end
    u.save!
    u
  end

  def friends_ids(tab_id)
    ret = tab_data(tab_id)['friends_ids']
    ret || search_for_friends(tab_id)
  end

  def search_for_friends(tab_id)
    data = FbGraph::Node.new('/me/friends', :access_token => tab_data(tab_id)['access_token']).fetch

    data = data.raw_attributes['data']
    set = {}
    data.each do |f|
      u2 = self.class.where(fbid: f['id']).first
      if u2 && u2.tab_data(tab_id)
        u2.tab_data(tab_id)['friends_ids'][self.id.to_s] = true if u2.tab_data(tab_id)['friends_ids']
        set[u2.id.to_s] = true
        u2.save!
      end
    end
    self.tab_data(tab_id)['friends_ids'] = set
    self.save!
    set
  end

  def create_tab_user(tab)
    begin
      ret = Tabs.const_get(tab.tab_type.classify).const_get('TabUser').create(tab_id: tab._id, user_id: self._id)
    rescue NameError => e
      return nil
      #raise 'wtf?? why tab_user is created if no user class?'
    end
    self.save!

    ret
  end

  def tab_user(tab = nil, create = false)
    tab = RequestStore.store[:co].tab unless tab
    ret ||= GTabUser.where(user: self, tab_id: tab._id).first
    if ret == nil && create
      ret = self.create_tab_user(tab)
      tab.tab_users << ret
      tab.save!
    end
    ret
  end

  def as_json(options)
    options.merge!(:only => [:_id, :name, :gender, :fbid])
    super(options)
  end


end