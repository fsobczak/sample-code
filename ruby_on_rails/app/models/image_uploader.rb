class ImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  storage :file

  attr_accessor :params

  def initialize(params)
    @params = params
    super
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  process :custom_size, if: :is_resize?

  def store_dir
    co = RequestStore.store[:co]
    "uploads/tabs/#{co.tab.id}/"
  end


  def is_resize? (model)
    @params[:resize]
  end

  def custom_size
    width =  @params[:width]
    height = @params[:height]
    if width > height
      resize_to_limit width, -1
    else
      resize_to_limit -1, height
    end
    #manipulate! do |img|
    #  img.convert "#{width}x#{height}"
    #  img
    #end
  end

  def filename
    if original_filename
      @name ||= Digest::MD5.hexdigest(File.dirname(current_path))
      "#{original_filename}-#{@name}.#{file.extension}"
    end
  end

end