class Page
  include Mongoid::Document
  #has_many :apps

  has_many :tabs

  embeds_one :plan
  accepts_nested_attributes_for :plan


  field :access_token, type: String
  field :created_at, type: Time, default: -> { Time.now }

  field :fbid, type: String
  field :name, type: String
  field :picture, type: String

  field :link, type: String
  field :is_published, type: Boolean
  field :likes, type: Integer

  field :empty, type: Boolean
  field :raw_tabs, type: Array
  field :deleted_apps
  field :whitelabel, default: false

  def contests
    tabs.select {|tab| tab.class.is_contest?}
  end

  def authorized(admin)
    return true if admin && admin.alien?
    pages = admin.pages
    if pages[self.fbid]
      true
    else
      admin.fetch_pages
      #raise admin.pages.inspect
      !admin.pages[self.fbid].nil?
    end
  end

  def is_whitelabel?
    self.whitelabel
    #['439011362835442'].include? self.fbid
  end

  def tabs
    Tab.where(page_id: self.id)
  end

  def update_access_token(admin)
    admin.fetch_pages
    raw = admin.raw_pages.detect { |raw| raw['id'] == fbid }
    self.access_token = raw['access_token']
    save!
  end

  def self.create_from_fbid(admin, fbid)
    page = Page.where(fbid: fbid).first
    unless page
      page = Page.new
      page.fbid = fbid

      page.update_access_token(admin)
      page.refresh_data
    end
    page
  end

  def install_tab(tab)
    fb_page = FbGraph::Page.new(self.fbid, :access_token => self.access_token)
    #return fb_page
    #raise tab.global_params.inspect
    #tab.delete
    fb_page.tab!(:app_id => tab.global_params[:fb][:id])
  end

  def admin_url(sub_page = 'tabs', opts = {})
    '/page/'+sub_page+'?page_fbid='+self.fbid
  end


  def refresh_data
    begin
      fb_page = FbGraph::Page.fetch(self.fbid, :access_token => self.access_token)
    rescue
      update_access_token(RequestStore.store[:co].admin)
    end

    fb_page.name # => 'smart.fm'
    fb_page.picture # => 'https://graph.facebook.com/smart.fm/picture'
    if fb_page.nil?
      self.empty = true
    else
      self.name = fb_page.name
      self.picture = fb_page.picture
      self.link = fb_page.link
      self.likes = fb_page.like_count
      self.is_published = fb_page.is_published
    end
    save!
  end

  def refresh_tabs
    begin
      data = FbGraph::Node.new("/#{self.fbid}/tabs", :access_token => self.access_token).fetch
    rescue
      update_access_token(RequestStore.store[:co].admin)
    end
    if data.raw_attributes
      data = data.raw_attributes['data']
    else
      self.raw_tabs = nil
    end
    self.raw_tabs = data
    save!
  end

  def as_json(options)
    co = RequestStore.store[:co]
    ret = super
    ret['pages'] = co.admin.pages
    ret
  end


  def fb_graph_page
    page = FbGraph::Page.new('FbGraph').fetch(
        :access_token => self.access_token,
        :fields => :access_token
    )
  end

  def di_testing

  end

end