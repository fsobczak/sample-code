class FileUploader < CarrierWave::Uploader::Base
  storage :file

  def store_dir
    co = RequestStore.store[:co]
    if co.page
      "uploads/#{co.page.id}/"
    else
      raise 'no page'
    end
  end

end