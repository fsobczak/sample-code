class Setting
  include Mongoid::Document
  embedded_in :tab

  field :comments, default: false
  field :analytics, default: nil

  field :feeds, default: {}
  field :invite, default: {}

  def on_create
    self
  end

  def save_logic(data)
    data.each do |k, v|
      case k
        when 'analytics'
          self.analytics = v
        when 'invite'
          self.invite.merge!(v)
      end
    end
  end

end