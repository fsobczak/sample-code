class Stats
  include Mongoid::Document
  embedded_in :tab

  ####
  # invites... shares... likes...? views... I CHUJ!!!... authorized? ... CZYLI defacto: new users
  #field :start_index, default: -> { date.to_time(:utc).to_i }
  #field :day, default: -> { Date.today }
  field :counts
  field :logs # (logs -> ips for unique visitors)

  index({ 'logs' => 1, 'counts' => 1}, { unique: true })

  def on_create
  end

  def key(date)
    date.to_time(:utc).to_i
  end

  # http://help.woobox.com/customer/portal/articles/953674-offer-stats-definitions
  # only VIEWS for now
  # type: views, visits... likes, entries [czyli maile], invites, shares, reach
  # some FUNNEL...
  # device: desktop, tablet, mobile... (only views). możliwe przedziały
  def add_one(type, count = 1, device, request)
    key = self.key(Date.today)
    counts = self.days['counts'][key]
    counts[type] += count
  end

  def ip2long(ip)
    long = 0
    ip.split(/\./).each_with_index do |b, i|
      long += b.to_i << ( 8*i )
    end
    long
  end

end