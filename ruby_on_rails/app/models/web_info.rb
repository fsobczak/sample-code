class WebInfo
  ROOT_DIR = "/home/social/si"
  FANPAGE_URL = "https://www.facebook.com/socialinvaders"

  #const ADMIN_WEB = 'https://social-invaders.com';
  ADMIN_WEB = ''

  BASIC_TABS = [
      {tab_type: 'static', class: :basic},
      {tab_type: 'youtube', class: :basic},
      #{tab_type: 'map', class: :basic},
      #{tab_type: 'contact', class: :free},
      {tab_type: 'topfans', class: :basic},

      #{tab_type: 'twitter', alien: true, class: :free},
      {tab_type: 'instagram_tab', alien: true, class: :basic},
    #{tab_type: 'files', alien: true, class: :free},
    #{tab_type: 'rss', alien: true, class: :free},
  ]

  CONTEST_TABS = [{tab_type: 'answer_contest', subtype: 'text', class: :contest},
                  {tab_type: 'answer_contest', subtype: 'image', class: :contest},
                  {tab_type: 'answer_contest', subtype: 'video', class: :contest},
                  {tab_type: 'answer_contest', subtype: 'multi', class: :contest},
                  {tab_type: 'answer_contest', subtype: 'instagram', class: :contest},
                  {tab_type: 'memo_contest', class: :contest},
  ]

  COMMERCE_TABS = [
      {tab_type: 'coupons', class: :commerce, alien: true},
  ]

  PRICING_LEVELS = [
      {fans: '<100', pl: 0, en: 0},
      {fans: '<1000', pl: 49, en: 15},
      {fans: '<5000', pl: 99, en: 29},
      {fans: '<50000', pl: 149, en: 45},
      {fans: '>50000', pl: 249, en: 75}
  ]

  def self.tabs_info
    ret = BASIC_TABS + CONTEST_TABS + COMMERCE_TABS
    ret.map do |tab|
      tab[:full_tab_type] = tab[:subtype] ? "#{tab[:tab_type]}.#{tab[:subtype]}" : tab[:tab_type]
      tab[:name] = I18n.t("tab.#{tab[:full_tab_type]}.name")
      tab[:description] = I18n.t("tab.#{tab[:full_tab_type]}.hint")
    end
    ret
  end

  def self.enter_admin
    if Rails.env.development?
      "http://rails.me/admin/pages?lang=#{I18n.locale}"
    else
      "https://socialinvaders.com/admin/pages?lang=#{I18n.locale}"
    end
  end

  def self.welcome_page
    'https://socialinvaders.com'
  end

  def self.host
    case Rails.env
      when 'development'
        'http://rails.me:3000'
      when 'staging'
        'http://dev.socialinvaders.com'
      when 'production'
        'https://socialinvaders.com'
    end
  end
end