class Looks::StyleFactory
  DEFAULT_FONTS = '"lucida grande", tahoma,verdana,arial,sans-serif';
  WEB_FONTS = ['Trebuchet MS', 'Arial', 'Verdana', 'Georgia']
  GOOGLE_FONTS = [
      'Open Sans', 'Oxygen', 'Lobster', 'Kaushan Script'
  ]

  attr_accessor :obj

  def initialize(obj)
    @obj = obj
  end

  def self.styles_links(obj)
    ret = Util.google_fonts_link(obj.options['google_fonts'])
    if obj.less_synchronized
      ret += "<link rel='stylesheet' type='text/css' href='#{obj.css_url}'/>"
    else
      ret += "<link rel='stylesheet/less' href='#{obj.less_url}'/>"
      ret += '<script type="text/javascript">less = { env: "development" };</script>' unless Rails.env.production?
      ret += '<script type="text/javascript" src="/js/less-1.3.3.min.js"></script>'
    end
    ret
  end

  def obj_less
    @obj.options['google_fonts'] = []
    code = ''

    if @obj.layout['type'] == 'frame'
      items = @obj.items
      code += code_design_item('main-color', items['main-color'])
      code += code_design_item('menu-font', items['menu-font'])
      code += code_design_item('text-font', items['text-font'])
      code += code_design_item('header-font', items['header-font'])
      if (!items['background-image']) # to chwilowy hack, mozna usunac
        items['background-image'] = ''
        @obj.save!
      end
      code += code_design_item('background-image', items['background-image'])
    elsif @obj.layout['type'] == 'minimal'
      items = @obj.items
      if (!items['background-image']) # to chwilowy hack, mozna usunac
        items['background-image'] = ''
        @obj.save!
      end
      code += code_design_item('text-font', items['text-font'])
      code += code_design_item('background-image', items['background-image'])
    end
  end

  def generate_less
    tab_type = @obj.tab_type
    req_list = %w(common helpers keyframes)
    output = ''
    req_list.each do |req|
      #raise Rails.root.inspect
      output += File.read(Rails.root + ('app/assets/less/'+req+'.less'))
    end

    req_list = %w(tab-bootstrap tab)
    req_list += ["#{tab_type}/#{tab_type}"]
    # tutaj czasami _less/contest?
    layout = @obj.layout

    req_list.push(@obj.layout['type'])

    req_list.each do |req|
      output += File.read(Rails.root + ('tab-less/'+req+'.less'))
      output += "\n\n"
    end
    output += obj_less

    f = File.new(less_file_path.to_s, "w")
    f.write(output)
    f.close
    @obj.less_synchronized = false
    @obj.less_timestamp = Time.now
  end

  def generate_css
    # TODO        rubyless?
  end

  def less_file_path(env = nil)
    if obj.class.name == "Looks::Design"
      Rails.root + prefix+ ('public/gen-less/tab-'+obj.tab.id+'.less')
    else
      Rails.root + prefix + ('public/gen-less/tpl-'+obj.id+'.less')
    end
  end

  private
  def embedd_font(f)
    if GOOGLE_FONTS.include? f
      @obj.options['google_fonts'] << f
    end
    "'#{f}', #{DEFAULT_FONTS}"
  end

  # TODO metody czekujace struktura, wlacznie z class.
  # moze arraydata sie przyda?
  # trzeba trzymac komplet zmiennych
  # @param [String] name
  def code_design_item(name, item)
    if item.is_a? String
      if name.ends_with?('font')
        return "@#{name}: #{embedd_font(item)};\n"
      end
      if name.ends_with?('image')
        return "@#{name}: '#{item}';\n"
      end
      return "@#{name}: #{item};\n"
    end
  end

end
