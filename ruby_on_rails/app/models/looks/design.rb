class Looks::Design
  include Mongoid::Document
  include Looks::Viewable

  embedded_in :tab

  field :less_timestamp, type: Time
  field :less_synchronized, type: Boolean, default: false

  field :items, type: Hash
  field :layout, type: Hash
  field :options, type: Hash, default: {google_fonts: {}, user_css: ''}

  def design_files
    self.tab.res['design']
  end

  def design_files=(val)
    self.tab.res['design'] = val
  end

  def on_create
    self.layout = self.tab.global_params[:default_layout]
    self.init_items
    self.generate_less
  end

  def tab_type
    self.tab.tab_type
  end

  def init_items
    list = ArrayData.design_items_list(self.tab)
    items = {}
    list.each do |k, v|
      items[k] = ArrayData.design_proto(self.tab, v)
    end

    if items['body']
      body = items['body']
      body['button']['enabled'] = 1
      body['text']['enabled'] = 1
    end
    self.items = items
  end


  def save_logic(data)
    data.each do |k, v|
      case k
        when 'layout'
          self.layout = v
        when 'items'
          self.items.deep_merge! v
          self.generate_less
        when 'images'
          self.images.deep_merge! v
        when 'options'
          self.options.deep_merge! v
        else
          raise StandardError('invalid save')
      end
      self.tab.save!
    end
  end
end