class Looks::BgPattern
  PREFIX = 'https://socialinvaders.com/images/bg/'
  LIST = %w(extra_clean_paper.png white.png
      gplaypattern.png grid_bg.png noisy_grid.png square_bg.png squairy_light.png subtle_grey.png whitey.png)

  def self.list
    LIST.map { |a| PREFIX + a }
  end

  def self.default(tab)
    if tab.design.layout['type'] == 'frame'
      PREFIX + 'squairy_light.png'
    else
      PREFIX + 'white.png'
    end
  end
end