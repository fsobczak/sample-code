module Looks::Viewable
  def generate_less
    sf = Looks::StyleFactory.new(self)
    sf.generate_less
    save!
  end

  def less_url
    return 'not generated' unless self.less_timestamp
    ts = self.less_timestamp.usec
    "/gen-less/tab-#{tab.id}.less?ts=#{ts}"
  end

  def css_url
    raise 'todo - potrzebne? moze z less_url do jednej metody'
  end
end